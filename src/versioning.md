# Versioning

How this knowledge base is versioned.

## Syntax

`[versionNumber]_[timestamp]`

- `timestamp`: date of the most recent change, formatted `DDMMYY`
- `versionNumber`: see below

Example: `1.11.1_030424`

## Version Numbering

- Major (1.x.x):
  - Significant functional changes
  - Major UI changes
- Minor (x.1.x):
  - Add/(re)move chapters
  - Structural changes
  - Significant updates of existing content
  - Minor UI changes
- Fix (x.x.1):
  - Smaller updates of existing content
  - Bugfixes
