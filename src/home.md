# Tom's Knowledge Base

This website, `TKB`, is a personal wiki/knowledge base. A bit of context about this project can be found in a short blog post: [My Personal Knowledge Base](https://ttntm.me/blog/personal-knowledge-base/).

The original content published on this site and its source code is publicly available under the [CC-BY-NC-SA-4.0](./license.md) license and can be found at [Codeberg](https://codeberg.org/ttntm/tkb).

## Main Topics

Main topics covered by this wiki:

- Software [Development](./development/index.html)
- Using [Linux](./linux/index.html)
- [Personal](./personal/index.html) Stuff
- Bookmarks, [Resources & Links](./resources.md)
- Notes on using [Software](./software/index.html)

## Usage Notes

- This wiki is a constant WIP; content review is pending for a lot of sections, and the site structure might change at any time
- Press `s` to use the search function
- Use the arrow keys to navigate to the next (`→`) and previous (`←`) pages
  - This kind of navigation works based on the order of the chapters outlined in the sidebar
- Links
  - Internal links are displayed in brackets: `[Link Name]`
  - External links are underlined and open in a new tab
- Version mgt.: see [Versioning](./versioning.md)

---

<div style="font-size: 0.75em; padding-top: .5rem;">
  <center>
    <img src="https://img.shields.io/badge/Version-{{ #include ../version.md }}-5E81AC" alt="Version {{ #include ../version.md }}"/>
  </center>
</div>
