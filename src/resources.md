# Resources

A list of all the pages in this wiki that are/have dedicated lists of resources and tools.

- [dev/resources-tools](./development/resources-tools.md)
- [js/resources](./development/javascript/resources.md)
- [linux](./linux/index.html)
- [nodejs/resources](./development/javascript/nodejs/resources.md)
- [personal/learning](./personal/learning.md)
- [software/privacy](./software/privacy.md)
- [vue/resources](./development/javascript/vue/resources.md)
- [webdev/resources](./development/webdev/resources.md)
