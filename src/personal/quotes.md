# Quotes

A small collection of noteworthy quotes from books, movies, music and tv series.

## Altered Carbon

> Nothing is sacred.
>
> Every gift we've been given, every resource discovered, each new and shining thing that catches our eye, we pollute.
>
> Disrespect.
>
> Violate.
>
> We tell ourselves this is progress.
>
> Selling each other the fruits of our destruction.
>
> Sparing no thought to what we loose or leave behind, churning in our wake.

_Takeshi Kovacs (Anthony Mackie) in S2E2: "Altered Carbon"_

## Donald Kingsbury on Tradition

> Tradition is a set of solutions for which we have forgotten the problems. Throw away the solution and you get the problem back. Sometimes the problem has mutated or disappeared. Often it is still there as strong as it ever was.

## Edsger W. Dijkstra on Programming

> If debugging is the process of removing software bugs, then programming must be the process of putting them in.

## Edward Snowden on Privacy

> Arguing that you don't care about the right to privacy because you have nothing to hide is no different than saying you don't care about free speech because you have nothing to say.

_Edward Snowden's "Ask Me Anything" on Reddit, May 21, 2015_ ([link](https://www.reddit.com/r/IAmA/comments/36ru89/comment/crglgh2/))

## Friedrich Nietzsche on Insanity

> In individuals, insanity is rare; but in groups, parties, nations and epochs, it is the rule.

## Hubert Reeves on God and Nature

> Man is the most insane species. He worships an invisible God and destroys a visible Nature. Unaware that this Nature he's destroying is this God he's worshipping.

## Leave the World Behind

> We fuck every living thing on this planet over and think it'll be fine because we use paper straws and order free-range chicken. And the sick thing is, I think deep down we know we're not fooling anyone.
>
> I think we know we're living a lie. An agreed upon mass delusion to help us ignore and keep ignoring how awful we really are.

_Amanda Sandford (Julia Roberts)_

## muesli on Crypto

> Cryptocurrencies are everything people don't know about computers combined with everything they don't understand about money.

_[muesli](https://elk.zone/mastodon.social/@fribbledom/112186459586487519)_ (March 30, 2024)

## Ralph E. Johnson on Software Architecture

> There is no theoretical reason that anything is hard to change about software. If you pick any one aspect of software then you can make it easy to change, but we don't know how to make everything easy to change. Making something easy to change makes the overall system a little more complex, and making everything easy to change makes the entire system very complex. Complexity is what makes software hard to change.

## Perturbator - "The New Black"

> I don't have to tell you things are bad. Everybody knows things are bad. It's a depression. Everybody's out of work or scared of losing their job. The dollar buys a nickel's worth. Banks are going bust. Shopkeepers keep a gun under the counter. Punks are running wild in the street and there's nobody anywhere who seems to know what to do, and there's no end to it. We know the air is unfit to breathe and our food is unfit to eat, and we sit watching our TVs while some local newscaster tells us that today we had fifteen homicides and sixty-three violent crimes, as if that's the way it's supposed to be
>
> We know things are bad - worse than bad. They're crazy. It's like everything everywhere is going crazy, so we don't go out anymore. We sit in the house, and slowly the world we're living in is getting smaller, and all we say is: "Please, at least leave us alone in our living rooms. Let me have my toaster and my TV and my steel-belted radials and I won't say anything. Just leave us alone."
>
> Well, I'm not gonna leave you alone. I want you to get mad!! I don't want you to protest. I don't want you to riot. I don't want you to write to your congressman, because I wouldn't know what to tell you to write. I don't know what to do about the depression and the inflation and the Russians and the crime in the street. All I know is that first you've got to get mad. You've got to say: "I'm a human being, goddammit! My life has value!"

## Westworld

> I think... humanity is a thin layer of bacteria on a ball of mud hurtling through the void. I think if there was a god, he would've given up on us long ago. He gave us a paradise, and we used everything up. We dug up every ounce of energy and burned it. We consume and excrete, use and destroy. Then we sit here on a neat little pile of ashes, having squeezed anything of value out of this planet, and we ask ourselves, "Why are we here?"
>
> You wanna know what I think your purpose is?
>
> It's obvious. You're here, along with the rest of us, to speed the entropic death of this planet. To service the chaos.
>
> We're maggots eating a corpse.

_William (Ed Harris) in S3E6: "Decoherence"_
