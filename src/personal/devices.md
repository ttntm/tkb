# Devices

Devices that I'm currently using.

| Device | Name | OS | Purpose | Acquired |
|---------|---------|---------|---------|---------|
| Apple iPhone Xs | `iPhone` | iOS 17.5.1 | Work phone | 10/2023 |
| Anker PowerCore+ 10050 | - | - | Powerbank | 2018 |
| Bose NC 700 | `Dark Star` | - | Headphones | 11/2022 |
| [Dell Precision M3800](../linux/setups/precision.md) | `helios` | [Manjaro](../linux/manjaro.md) | Private computer | 02/2014 |
| Google Pixel 4a 5G | `pxl` | Android 14 | Private phone | 03/2021 |
| [Lenovo ThinkPad P14s](../linux/setups/thinkpad.md) | `blackwall` | [Fedora](../linux/fedora.md) | Work computer | 10/2023 |
| Minisforum Mini-PC | `mediabrick` | Windows 10 | Media/Streaming | 07/2021 |
| PlayStation 5 (Disk Edt.) | - | latest | Gaming | 06/2021 |
| [Samsung Q60T 58"](https://www.samsung.com/at/support/model/QE58Q60TAUXZG/) | - | 2501.1 | TV | 01/2021 |
| Seagate Expansion+ 4TB | `SG-XT` | - | Private drive | 07/2020 |
