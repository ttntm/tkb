# Food

## Recipes

See: [recept0r](https://recept0r.com)

## Resources

- [nahgenuss](https://www.nahgenuss.at) (order meat/fish directly from farmers)
- [farmshops.eu](https://farmshops.eu) (a map of farm shops and vending machines)
- [FEUERzeug](https://www.feuer-zeug.at) (bio chili sauces)
