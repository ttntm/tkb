# Ethics

## CCC Hacker Ethics

- Access to computers - and anything which might teach you something about the way the world really works - should be unlimited and total. Always yield to the Hands-On Imperative!
- All information should be free.
- Mistrust authority - promote decentralization.
- Hackers should be judged by their acting, not bogus criteria such as degrees, age, race, or position.
- You can create art and beauty on a computer.
- Computers can change your life for the better.
- Don't litter other people's data.
- Make public data available, protect private data.

Source: [CCC Hacker Ethics](https://www.ccc.de/en/hackerethics)

## 1x Engineer

A non-exhaustive list of what qualities make up a 1x engineer: [1x.engineer](https://1x.engineer)
