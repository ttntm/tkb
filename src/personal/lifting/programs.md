## Programs

## GZCLP

The source:

- [The GZCL Method for Powerlifting](https://swoleateveryheight.blogspot.com/2012/11/the-gzcl-method-for-powerlifting.html)
- [ The GZCL Method, Simplified](https://swoleateveryheight.blogspot.com/2014/07/the-gzcl-method-simplified_13.html)

Basics: [The GZCLP Program: Linear Progression For Beginners](https://www.saynotobroscience.com/gzclp-infographic/)

Infographic: [GZCLP](img/GZCLP.jpg)

Restarting cycles:

- T1: test for 5RM, use 85% of that for the new cycle
- T2: +10kg based on last cycle start

T3 weight increase: after 25 reps on the last set

Wiki: [r/gzcl/wiki/gzclp](https://www.reddit.com/r/gzcl/wiki/gzclp/)

Adding more T3s: [Guide to expanding GZCLP](https://www.reddit.com/r/gzcl/comments/ey97l4/resource_guide_to_expanding_gzclp_for_novices/)

## Starting Strength

Work sets: 3x5r
Exception: Deadlift - use 1x5r for work sets

Which leads to:

- No weight - 2x5r
- 50-60% 1RM - 1x3r
- 80% 1RM - 1x2r
- Work set - 3x5r

### Group A

1. Squat
2. Press
3. Deadlift

### Group B

1. Squat
2. Bench press
3. Deadlift

## Future

Might be helpful:

- [Black Iron Beast: Starting Strength](https://blackironbeast.com/starting-strength)
- [Boring But Big ](https://www.jimwendler.com/blogs/jimwendler-com/101077382-boring-but-big)
- [The 5/3/1 Philosophy for Beginners](https://www.jimwendler.com/blogs/jimwendler-com/101065094-5-3-1-for-a-beginner)
