# Exercises

Things that I had to look up:

- [Bent-Over Dumbbell Row](https://www.garagegymreviews.com/dumbbell-rows)
- [JM Press](https://www.roypumphrey.com/dumbbell-jm-press/)
  - Also: [Let the JM Press Be Your Key to Bigger and Stronger Triceps](https://barbend.com/jm-press/)
- [Romanian Deadlift](https://outlift.com/romanian-deadlift/)

## Arms

- [8 Tips to Build Bigger Arms](https://www.menshealth.com/uk/building-muscle/a754267/8-tips-to-build-bigger-arms/)
- [The #1 Arm Workout](https://builtwithscience.com/workouts/arm-workout/) (see image below)
- [The Guide to Arm Training for Powerlifters](https://barbend.com/arm-training-for-powerlifters/)

![arm-workout-routine](img/arm-workout-routine.webp)

NEVER use a straight bar for curls and/or skull crushers.

Prefer dumbbell skull crushers.

Biceps and triceps isolation exercises should be done as "fatigue" movements rather than "progressive tension" movements (in the 8-15 rep range, maybe even 10-15 reps).

These muscles respond quite well to this sort of training, especially when combined with the heavier lower rep work they indirectly get plenty of during all of your compound pressing and pulling exercises.

The result: better looking arms and healthier elbows.

Help with elbow pain: [Dodgy Elbows](https://drjuliansaunders.com/dodgy-elbows/)

## Assistance Exercises

From stronglifts: [Assistance Work for Arms & Abs](https://stronglifts.com/stronglifts-5x5/assistance-work/)

In general, some of the best assistance exercises for each area:

- Abs
  - sit ups
  - ab wheel roll-outs
  - hanging leg raises
- Low Back
  - good mornings
  - back raises
  - reverse hyper extensions
- Quads
  - lunges
  - leg presses
- Chest
  - dips
  - dumbbell presses
  - dumbbell flyes
- Triceps
  - dumbbell presses
  - dips
  - triceps extension/pushdowns
  - skull crushers
  - jm press
- Shoulders
  - any pressing exercise
- Hamstrings
  - glute ham raise
  - good mornings
  - back raises
  - leg curls
- Lats/upper back
  - pull-ups
  - bent rows
  - dumbbell rows
  - shrugs
  - face pulls
