# Weight Lifting

Currently giving [GZCLP](#gzclp) a try (mid July '24) - let's see how it works out.

--

[The Complete Strength Training Guide](https://www.strongerbyscience.com/complete-strength-training-guide/)

As a general rule, you need to try to add weight to the work sets of the exercise every time you train, until you can't do this anymore.
This is the basic tenet of "progressive resistance training".

For all shoulder work, presses like bench etc.: [Retract the fucking scapula](https://www.youtube.com/watch?v=JJ5iCcKzg2Q) & [Sloppy Shoulders and Bad Rows](https://www.roypumphrey.com/sloppy-shoulders-and-bad-rows/)

## Pages

- [Exercises](./exercises.md)
- [Issues](./issues.md)
- [Programs](./programs.md)

## Diet

[How to Eat for Muscle Growth](https://bonytobeastly.com/bulking-diet-guide/)

## Form

Make sure that hands, wrists, forearms and elbows stay in line!

Stop ever so slightly before reaching that dead hang/completely locked out position on pull-ups and lat pull-downs - helps avoid inner elbow pain.

### Breathing

> In fact, it is a good practice to take and hold the biggest breath you can before each rep of your heaviest sets.
>
> Get in the habit of breathing correctly during your lighter sets so that the pattern is well established by the time the weights get heavy. The Valsalva maneuver will prevent far more problems than it has the potential to cause. It is a necessary and important technique for safety in the weight room.

_SS, pg. 85_

**Bench Press / Squat**: In at the top, keep breath, top off at the top before next rep.

### Grip

Except for the squat, there is no thumbless grip in barbell training.

### Weak Points

See: [Compendium to Overcoming Weak Points](https://old.reddit.com/r/weightroom/comments/7vwodf/compendium_to_overcoming_weak_points/)

## Recovery

The time between sets will vary, in a couple of ways, with the conditioning level of the athlete.

Novices are not typically strong enough to fatigue themselves very much, and they can go fairly quickly, just a minute or two, between sets, since they are not lifting much weight anyway. The first two or three sets can be done as fast as the bar can be loaded, especially if two or more people are training together.

More advanced trainees need more time, perhaps 5 minutes, between the last warm-ups and the work sets. If they're doing sets across, very strong lifters may need 10 minutes or more between work sets.

## Warmup

Practice movement before weight gets heavy!

The warm-up sets serve only to prepare the lifter for the work sets; they should never interfere with the work sets.

Use with 5-3-2 reps, i.e. empty bar, 50-60% 1RM, 80% 1RM

Also:

- [How to Warm Up For Powerlifting](https://powerliftingtechnique.com/how-to-warm-up-for-powerlifting/)
- [Limber 11: Lower-Body Warm-Up](https://www.bodybuilding.com/fun/limber-11-the-only-lower-body-warm-up-youll-ever-need.html)
