# Issues

## Anterior Pelvic Tilt

Yup, I do most likely have that.

Try to fix using these guidelines:

- [How to Fix Anterior Pelvic Tilt in 4 Simple Steps](https://builtwithscience.com/workouts/anterior-pelvic-tilt/)
- [How To Fix Anterior Pelvic Tilt](https://www.youtube.com/watch?v=5J8RIIvEj6k)
- [Perfect Posture in 5 Steps](https://www.youtube.com/watch?v=TQqgf8kB6R8)

![anterior-pelvic-tilt-corrective-routine](img/anterior-pelvic-tilt-corrective-routine.webp)

## Hip Pain

Most noticeable as an inner thigh pain when squatting, or doing deadlifts.

Might be weak/tight hip flexors - try to stretch them. Also, stance might be too narrow.

Hip mobility:

- [How To Increase Hip Mobility For Squats](https://powerliftingtechnique.com/hip-mobility-for-squats/)
- [How to Tell if You Have Tight Hip Flexors and the 3 Best Ways to Loosen Them ](https://www.livestrong.com/article/13764569-tight-hip-flexors/)

Another likely reason: not using enough hip drive.

For hip drive, try to focus on what the legs are doing - either spread the floor with your feet or "twist" them (in such a way that your toes are twisted outward). Also try to push your knees out. To test how this should feel like: stand with your feet facing straight and flex your your glutes as tight as you can. Then point your feet outward 30 degrees. Flex again as hard as you can, and you will notice you can flex harder. Now while flexing and feet planted "twist" your feet(toes in opposite directions) and you can flex harder. If the "twist your feet" cue doesn't work for you, try to spread the floor with your legs.

Could also be a loss of tension at the bottom of the squat:

> By not keeping the glutes engaged in the bottom position, we will end up in a position where the quads and lower back will try to extend the hips in order to stand back up out of the hole. We do not want this to happen because they are not built for this role, and by allowing these muscles to take over - we increase risk of injury.

No. 1 reason for that: a lack of "breathing and bracing" technique.
See also: [Any tips on getting my core/brace stronger?](https://old.reddit.com/r/weightlifting/comments/109i5qi/any_tips_on_getting_my_corebrace_stronger/)

To fix, try:

1. Pull your belly button into your spine (don't lose this throughout the remaining steps)
2. Take a large breath into the abdomen utilizing your diaphragm (cue: big air)
3. Hold this breath and create pressure in the abdomen by forcefully exhaling WITHOUT letting any air out (this process is called intra-abdominal pressure)
4. Brace your core by pushing your entire trunk out in a 360-degree fashion (front abs, side abs, and low back)
5. Squeeze hard and visualize that someone is going to punch you in the stomach and you're "bracing for impact"

From: [How to Fix Losing Tension At Bottom of Squat](https://powerliftingtechnique.com/fix-losing-tension-squat/)

For deadlifts: make sure to do them with proper form - see: [Deadlift with Proper Form](https://stronglifts.com/deadlift/)

Further reading: [The Ultimate Guide to Getting Out of Hip Pain and Back to Squatting, Deadlifting and Olympic Lifts](https://fitnesspainfree.com/2021/07/the-ultimate-guide-to-getting-out-of-hip-pain-and-back-to-squatting-deadlifting-and-olympic-lifts/)

12/2024: significantly better after buying and using proper lifting shoes. Next steps are to re-learn proper breathing/bracing and fixing squat/deadlift form.

01/2025: slight discomfort after T1 deadlifts (60 kg) - might be insufficient glute activation, or worse, [Femoral Anterior Glide Syndrome](https://ericcressey.com/newsletter150html/) - should keep an eye on things and keep those glutes tight on the way up.
Try
