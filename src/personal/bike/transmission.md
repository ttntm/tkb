# Schaltung

## Kurzanleitung: Schaltung Einstellen

[Fahrradschaltung einstellen](https://www.bergzeit.de/magazin/fahrrad-schaltung-einstellen/#uumwerfer)

1. Schaltwerk reinigen und kontrollieren.
2. Einstellung des Schaltwerks:
  1. **Unteren Endanschlag des Schaltwerks einstellen:** Schaltung vorne auf größtes Kettenblatt und hinten auf kleinstes Ritzel stellen – Schaltzug mit Inbusschlüssel lösen – Schaltwerksröllchen parallel zu kleinstem Ritzel stellen (Schraube H).
  2. **Oberen Endanschlag des Schaltwerks einstellen**: Vorne auf kleinstes Kettenblatt schalten – Schaltwerk mit Fingern Richtung größtes Ritzel drücken – Schaltwerksröllchen sollte mit dem größten Ritzel fluchten (Schraube L).
  3. **Winkeleinstellung:** Einstellung kleinstes Kettenblatt/größtes Ritzel – mit B-Schraube Vorspannfeder und damit die Höhe des oberen Schaltröllchens verstellen – Abstand größtes Ritzel-Schaltröllchen sollte ca. 1 bis 1,5 Kettenglieder betragen.
  4. **Spannung des Schaltseils**: Kette auf großes Kettenblatt legen – Einstellschraube für die Zugspannung am Schalthebel oder am Schaltwerk fast ganz hineindrehen – Schaltseil nachziehen und am Schaltwerk festschrauben.
  5. **Einstellung der Zugspannung**: Auf mittleres Kettenblatt schalten – Gänge einzeln durchschalten – Spannung gegebenenfalls etwas erhöhen oder etwas entlasten.
3. Einstellung des Umwerfers:
  1. **Richtige Höhe des Umwerfers einstellen**: Der Abstand zwischen Leitblech und den Zähnen des großen Kettenblatts sollte 1 bis 3 Millimeter betragen. **Testen**: hinten Mitte + vorne Mitte schalten
  2. **Unteren Endanschlag des Umwerfers einstellen**: Kette auf kleinstes Kettenblatt – Schaltzug lösen – L-Schraube verstellen, bis die Kette das Leitblech gerade nicht mehr berührt.
  3. **Oberen Endanschlag des Umwerfers einstellen**: Kette auf größtes Kettenblatt und kleinstes Ritzel legen – mit der H-Schraube so einstellen, dass die Kette das Leitblech gerade nicht mehr berührt.
  4. **Zugspannung einstellen**: Kette auf mittleres Blatt und größtes Ritzel legen – Zugspannung so lange verändern, bis die Kette nicht mehr an der Innenseite des Leitblechs schleift.

- [RABE Bike TechHilfe: Schaltung einstellen](https://www.rabe-bike.de/de/magazin/schaltung-einstellen)
- [Die Fahrrad Gangschaltung richtig einstellen](https://www.rabe-bike.de/de/magazin/fahrrad-schaltung-einstellen)
- [Shimano Schaltung einstellen: Anleitung für Schaltwerk und Umwerfer](https://www.bike-magazin.de/werkstatt/schaltung-antrieb/shimano-schaltung-einstellen-anleitung-fuer-schaltwerk-und-umwerfer/)

## Vorne

1. Auf vorne = kleinstes, hinten = größtes Radl schalten
2. Links am Schalthebel die Schraube für die Spannung ganz reindrehen (im Uhrzeigersinn)
3. Höhe/Winkel prüfen und ggf. einstellen - aufpassen, kann schlimmbessern!
4. Schalt-Seil Fixierung lösen
5. L-Begrenzung einstellen
6. Schalt-Seil leicht straffen und wieder fixieren
  1. **Achtung** - Fein-Einstell-Schraube am Schalthebel: wurde in Schritt 2 ganz reingedreht, jetzt **vor Fixierung 6-7 Umdrehungen *heraus* drehen** - damit sollte dann nach Fixierung des Seilzugs das Fine-Tuning in beide Richtungen mögl. sein
7. H-Begrenzung einstellen (alle Gänge max. schalten)
8. Auf das mittlere vordere Rad schalten, hinten auf 1 bleiben
9. Mit der Schraube am Schalthebel: Kette innen so nah wie möglich an den Umwerfer bringen (ohne dass es schleift!)
  1. Raus ⇒ mehr Spannung
  2. Rein ⇒ weniger Spannung

**Wenn es schiefgeht**: alles nochmal machen.

Bei Schleifen: Höhe/Winkel der Schelle vom Umwerfer prüfen und ggf. richten

- [SCHALTUNG: Shimano-Umwerfer korrekt justieren](https://www.bike-magazin.de/werkstatt/schaltung-antrieb/schaltung-shimano-umwerfer-korrekt-justieren/)
- [Einstellen von Shimano Umwerfern: 13 Schritte (mit Bildern) – wikiHow](https://de.wikihow.com/Einstellen-von-Shimano-Umwerfern)
- [Front Derailleur Adjustment](https://www.parktool.com/en-int/blog/repair-help/front-derailleur-adjustment)

## Hinten

tbd
