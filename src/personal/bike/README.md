# Bike

## Info

- Brand: Merida
- Type: MTB XC HT
- Model year: 2006
- Wheel diameter: 26"
- Frame: Matts TFS 7005
- SR Suntour XCM-H RL fork, 80mm
- Shimano Deore derailleur
- Shimano XT Conventional derailleur
- Shimano CS-HG50-9 cassette (11-32 z.)
- Shimano CN-HG73 chain
- FSA Alfa Drive 44 Powerdrive cranks
- XC Alloy pedals
- X-Mission Comp stem
- X-Mission Comp Flat 600 handlebars
- Hayes HFX-9 180mm front brake
- Rear Hayes HFX-9 160mm brake
- Hayes HFX-9 brake levers
- Front hub: Shimano M475
- Alex Rims DP17 Disc

## Pages

- [Bremsen](./brakes.md)
- [Schaltung](./transmission.md)

## Resources

- [Anleitung: Fahrradsattel einstellen](https://www.diamantrad.com/blog/fahrradsattel-einstellen/)
- [Fahrradgriffe lösen und montieren](https://www.fahrrad-xxl.de/beratung/fahrrad-reparatur/fahrradgriffe-wechseln/)
- [Lenkerklemmmaße (Tabelle)](https://wikipedalia.com/index.php/Lenkerklemmma%C3%9Fe_(Tabelle))
- [Wikipedalia](https://wikipedalia.com)
