# Bremsen

**Manual**: [hfx9.pdf](https://drive.proton.me/urls/XGWZ00N8YR#wIErZPePA1fk)

[Workshop: Wartung, Einstellung und Pflege einer Scheibenbremse](https://www.rabe-bike.de/de/magazin/workshopwartung-einstellung-und-pflege-von-scheibenbremsen)

> Wenn die Beläge dünner werden, empfehlen wir, sie zur Sichtprüfung auszubauen. Dazu zunächst das Laufrad ausbauen. Als erstes sollten die Beläge in ihre maximale Ausgangsposition zurück gedrückt werden. Dazu mit einem flachen und stumpfen Hebel zwischen die Beläge fahren und diese mit sanfter Gewalt auseinander drücken. Es eignet sich zum Beispiel ein dünner Reifenheber (Ein scharfkantiger Schraubendreher kann die Beläge beschädigen!). Das Zurückdrücken der Kolben ist auch nötig, damit bei Bedarf neue Beläge im Sattel genügend Platz finden.

### Bremsen entlüften

1. Zugehörigen Bremshebel so stellen, dass der Vorderteil direkt nach oben zeigt
2. Kleine Adeckung vom Entlüftungs-Loch runternehmen
3. Trichter + Küchenrolle anbringen
4. Bei Bremse: kleine Abdeckung runternehmen
5. Alles mit Küchenrolle abdecken, v.a. die Bremsklötze
6. Sicherstellen, dass die kleine Öffnung aufgeht
7. Spritze + Schlauch mit Bremsflüssigkeit füllen
8. Anbringen
9. Schraube lösen, Flüssigkeit durchdrücken
10. Wenn fertig, dann alles wieder zumachen - **nicht auf den kleinen Stöpsel am Bremshebel vergessen!**

- [Hayes Scheibenbremsen entlüften & Bremsleitung kürzen](https://www.fahrrad-workshop-sprockhoevel.de/scheibenbremse-entlueften-hayes-video.htm)
- [Scheibenbremsen nachfüllen](https://www.mtb-news.de/forum/t/scheibenbremsen-nachfuellen.502905/)
- [hayes entlüften ohne set???](https://www.mtb-news.de/forum/t/hayes-entlueften-ohne-set.184677/?t=184677&highlight=Hayes+entl%FCften)
- [hayes hfx 9 (first bleed)](https://ridemonkey.bikemag.com/threads/hayes-hfx-9-first-bleed.224876/)

### Beläge wechseln

Das ganze Prozedere:

<https://www.youtube.com/watch?app=desktop&v=BonKT8NdF3M>

Rad raus:

<https://www.youtube.com/watch?app=desktop&v=GUi7uy2C3_g>

Links, Probleme etc.:

[Hayes HFX9 - Beläge wechseln - Kolben lassen sich nicht zurückdrücken](https://www.mtb-news.de/forum/t/hayes-hfx9-belaege-wechseln-kolben-lassen-sich-nicht-zurueckdruecken.343810/)

### Bremsscheibe schleift

1. Halterung Rahmen > Bremssattel lösen (2 Schrauben)
2. Bremsen + halten (z.B. Gummiband)
3. Schrauben gleichmäßig wieder festziehen; 1/4 Umdrehung, dann nä. Schraube usw.

Falls nicht besser:

1. Schnell-Schraube vom hinteren Rad lösen
2. Festziehen & fixieren
3. …dann die Punkte vom oben nochmal

Wenn Bremsscheibe verzogen ist: versuchen, sie wieder gerade zu biegen (Zange!)
