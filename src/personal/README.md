# Personal

Personal topic index page.

## Pages

- [Bike](./bike/index.html)
- [Devices](./devices.md)
- [Ethics](./ethics.md)
- [Food](./food.md)
- [Learning](./learning.md)
- [Quotes](./quotes.md)
- [Senior Engineer Checklist](./senior-engineer-checklist.md)
- [Weight Lifting](./lifting/index.html)
- [Writing](./writing.md)

## Links

- [Career Ladders](https://career-ladders.dev) (re: engineering management)
- [GNU Kind Communications Guidelines](https://www.gnu.org/philosophy/kind-communication.html)
- [Mental Models: The Best Way to Make Intelligent Decisions](https://fs.blog/mental-models/)
- [The Programmer's Brain](https://yoan-thirion.gitbook.io/knowledge-base/software-craftsmanship/the-programmers-brain) (a summary of the book)
