# A Senior Engineer's Checklist

Source: [littleblah/senior-engineer-checklist](https://github.com/littleblah/senior-engineer-checklist)

| Task | Effort | Category | Career Impact | Company Impact |
|------|------|------|------|------|
| 1 | Understand the business aspect of your work, and what makes money. Eventually, only that matters. | high | leadership | high |
| 2 | Get involved with hiring for your team and company, and maintain a high bar for hiring quality candidates. | medium | hiring | low |
| 3 | Design and develop systems appropriate to scale, extensibility, and scope of the problem. Avoid over-engineering | high | technology | medium |
| 4 | Question everything and ask "why" repetitively until you get to the root of problems and situations. | high | technology | medium |
| 5 | Demand accountability and ownership from others. | high | leadership | low |
| 6 | Once you understand the company's needs, lead at least one high-impact project with a clear definition and target of successful delivery. | high | leadership | high |
| 7 | Work towards disambiguating ambiguous problem statements. | high | leadership | medium |
| 8 | Cultivate relationships with other teams and develop trust. | high | network | medium |
| 9 | Do not be adamant about your views. Listen to others and accept that there is more than one way to look at a problem statement, and multiple valid solutions to a problem.| medium | network | medium |
| 10 | Be involved with multiple projects as a consultant, a reviewer and/or a mentor. | medium | network | medium |
| 11 | Follow the principles of extreme ownership. | high | leadership | high |
| 12 | Have strong mentors to help you navigate and grow in the company. | high | mentor | high |
| 13 | Take projects with high risk and high rewards. | high | growth | high |
| 14 | Strive for deep technical expertise in technologies used in your team. | high | growth | high |
| 15 | Ask for stretch projects from your manager, or help her identify one for you. | medium | growth | high |
| 16 | Discuss the goals of your manager, and how you align your work with it. | medium | managers | high |
| 17 | Invest time in networking effectively with seniors, peers, and juniors. | medium | network | high |
| 18 | Be a mentor to a couple of junior engineers. | medium | mentor | low |
| 19 | Increase your breadth of knowledge in the domain of your team/company. | high | growth | high |
| 20 | Drive your one-on-ones. Maintain a list of topics for the next one-on-one discussion. | medium | one-on-one | high |
| 21 | Discuss problems with your manager, but have some solutions beforehand. | medium | managers | high |
| 22 | Increase your breadth of knowledge in technology.| high | growth | high |
| 23 | Explore emerging technologies by building small prototypes. | high | growth | medium |
| 24 | Read a few technical books every year. | high | growth | high |
| 25 | Before suggesting the next big shiny technology for your production stack, understand its pros and cons thoroughly. | high | technology | medium |
| 26 | Schedule a regular one-on-one with your manager | low | one-on-one | high |
| 27 | Schedule a regular one-on-one with your skip level manager | low | one-on-one | high |
| 28 | [Reminder] One-on-one usually is not a status meeting | medium | one-on-one | high |
| 29 | Involve the manager in your personal life (just a little though) | low | managers | low |
| 30 | Actively seek feedback from your manager | low | managers | high |
| 31 | Keep your manager up-to-date in things you are involved with, but don't get bogged down in unnecessary detail | low | managers | high |
| 32 | Keep your manager up-to-date in things you are blocked on | low | managers | high |
| 33 | Keep your manager up-to-date on people you have difficulty working with | medium | managers | high |
| 34 | Give constructive feedback to your manager | medium | managers | high |
| 35 | If you are overworked, let your manager know | low | managers | high |
| 36 | If you are under-utilized, ask your manager for areas to explore | medium | managers | high |
| 37 | If you have an ineffective or neglectful manager, talk to your manager about your expectations | low | managers | high |
| 38 | If you have a micromanager, talk to your manager about your expectations | low | managers | high |
| 39 | If you have an abusive manager, talk to your skip manager or HR with data points | low | managers | high |
| 40 | If you have an ineffective skip manager and ineffective manager, switch the team or company | high | managers | high |
| 41 | If you do not have a cordial relationship with your manager, switch the team or company | high | managers | high |
| 42 | [Reminder] Leverage = impact produced/time invested. Use leverage as a yardstick for effectiveness | high | growth | high |
| 43 | Measure what you want to improve. Make efforts measurable | medium | growth | high |
| 44 | Maintain high visibility of projects which have a high risk | high | growth | high |
| 45 | To deal with difficult folks, discuss with your managers and mentors | low | network | low |
| 46 | To deal with difficult folks, fall back to first principles | low | network | low |
| 47 | Be reachable to other engineers | low | network | low |
| 48 | Have a huge bias for action and delivery, but do not over-compromise on quality. Push back if required | high | leadership | medium |
| 49 | Simplify code, systems, and architectures relentlessly | high | technology | low |
| 50 | Demand high-quality work from others, but be pragmatic | medium | technology | low |
| 51 | Prioritize fixing tech-debt in code, systems, and architecture when the incremental cost to develop keeps rising| high | technology | low |
| 52 | Document extensively, and demand it from others. Document "why" more than "how" | high | technology | low |
| 53 | Avoid politics, but have right folks vouch for your work | high | politics | medium |
| 54 | When dealing with politics, fall back to first principles | high | politics | low |
| 55 | If politics thrives due to team or company culture, switch | high | politics | high |
| 56 | Try not to get involved in office gossip | low | politics | medium |
| 57 | Avoid stretching yourself too thin to be effectiv| medium | leadership | medium |
| 58 | Respect code and systems that came before you. There are reasons for every code and every guard that exists in production | low | technology | low |
| 59 | Before you suggest major refactors, ensure you understand the system deeply | medium | technology | medium |
| 60 | Resist the urge to refactor major systems to achieve simplification, because there's a risk you will end up with a similarly complex system after some time | medium | technology | medium |
