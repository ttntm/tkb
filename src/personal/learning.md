# Learning

A list of things to learn, practice and try out.

- [ ] [Algorithmic Thinking](https://labuladong.gitbook.io/algo-en)
  - Articles about different kinds of Algorithmic Thinking, all based on LeetCode problems
- [x] [Back End Development and APIs](https://www.freecodecamp.org/learn/back-end-development-and-apis/)
- [ ] Browser extensions with [Extension](https://extension.js.org)
- [ ] CSS
  - [ ] [clip-path](https://codepen.io/stoumann/pen/abZxoOM)
  - [ ] [container queries](https://ishadeed.com/article/css-container-query-guide/)
  - [ ] [Popover API](https://web.dev/blog/popover-api)
    - [ ] [Anchor Positioning and the Popover API](https://css-irl.info/anchor-positioning-and-the-popover-api/)
  - [ ] [scroll-driven animations](https://scroll-driven-animations.style)
- [ ] Electron
- [ ] [FCC's Mathematics playlist](https://www.youtube.com/playlist?list=PLWKjhJtqVAbl5SlE6aBHzUVZ1e6q1Wz0v)
- [ ] [Hack Design](https://hackdesign.org)
- [ ] [Kubernetes](../development/devops/kubernetes.md)
  - [ ] "Production Kubernetes" (O'Reilly, 2021)
- [ ] Rust
  - [ ] [The Rust Programming Language](https://doc.rust-lang.org/book/ch00-00-introduction.html)
  - [ ] [Interactive Rust Language Tutorial on Replit](https://www.freecodecamp.org/news/rust-in-replit/)
  - [ ] [rustlings](https://rustlings.cool)
  - [ ] [blessed.rs](https://blessed.rs)
    - An unofficial guide to the Rust ecosystem
  - [ ] [RustBooks](https://github.com/sger/RustBooks)
- [ ] [Seeing theory](https://seeing-theory.brown.edu)
- [ ] Web Components
  - [ ] [Web Components and You (Part 1): The Shadow Dom](https://jschof.dev/posts/2024/2/web-components-and-you-1/) + the rest of the series

Find books at:

- [Anna's Archive](https://annas-archive.org)
- [Library Genesis](https://libgen.li)
- [SoftArchive](https://sanet.st/full/)
