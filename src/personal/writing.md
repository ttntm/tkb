# Writing

Notes on writing.

## Links

- [Landing Page Copywriting](https://www.julian.com/guide/startup/landing-pages)

## Tips

1. Avoid alliteration. Always.
2. Prepositions are not words to end sentences with.
3. Avoid clichés like the plague. (They're old hat.)
4. Eschew ampersands & abbreviations, etc.
5. One should never generalize.
6. Comparisons are as bad as clichés.
7. Be more or less specific.
8. Sentence fragments? Eliminate.
9. Exaggeration is a billion times worse than understatement.
10. Parenthetical remarks (however relevant) are unnecessary.
11. Who needs rhetorical questions?

Source: <https://elk.zone/aus.social/@dgar/111245156526566831>

## Tools

- [ahrefs writing tools](https://ahrefs.com/writing-tools/)
