# Gnome

Notes on Gnome customization, functionality etc.

## Install Extensions

<https://www.fossmint.com/install-gnome-shell-extensions/>

Extract and move to your `~/.local/share/gnome-shell/extensions` directory.

Next, open `metadata.json` file inside it and check for the value of uuid and make sure it is the same value as extension folder's name. If it isn't, rename the folder to the value of the `uuid` property.

Next, restart the GNOME Shell by pressing `Alt+F2` and entering `r`.

## Transparency

- <https://extensions.gnome.org/extension/1011/dynamic-panel-transparency/>
- <https://extensions.gnome.org/extension/982/glassy-gnome/>

## Blur

<https://github.com/aunetx/blur-my-shell>

## Looking Glass

On Gnome-Shell you can use the built-in tool [looking glass](https://wiki.gnome.org/Projects/GnomeShell/LookingGlass).

Press `Alt+F2`, type `lg` and press enter.

Example: getting `WM_CLASS` for open windows

## Network Settings

### MAC Adress Options

- **Preserve**: Keep the MAC address at boot-time. Don't change from the set MAC address.
- **Permanent**: Use the genuine hardware MAC address.
- **Random**: Generate a random MAC address.
- **Stable**: Generate a stable, hashed MAC address. Every time the connection activates, the same fake MAC address is used. This can be useful in cases where you want to hide your hardware MAC address, but you need to get the same IP address from a DHCP router.

## Regenerate failed thumbnails

`rm -rf ~/.cache/thumbnails/fail/gnome-thumbnail-factory`

## Rounded Window Corners

<https://github.com/yilozt/rounded-window-corners>

## WezTerm + Cursors + Wayland

There is a known issue with that, see: <https://github.com/wez/wezterm/issues/1742>
