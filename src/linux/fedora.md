# Fedora

## System Configuration

### Battery

Use TLP.

[Fedora — TLP 1.6 documentation](https://linrunner.de/tlp/installation/fedora.html)

Alternative: [power-profiles-daemon](https://gitlab.freedesktop.org/upower/power-profiles-daemon)

### Bluetooth

Always on when booting, despite settings in BT config.

Workaround:

- Off by default: `sudo systemctl disable bluetooth.service`
- On when needed: `sudo systemctl start bluetooth.service`

### Gnome

#### Extensions

Stored in: `/home/USER/.local/share/gnome-shell/extensions`

1. Download
2. Unpack
3. Copy to folder in /extensions
4. Rename folder based on UUID in `metadata.json`
5. Restart Gnome Session

#### Icon Cache

Clear: `gtk4-update-icon-cache`

Example: `gtk4-update-icon-cache ~/.local/share/icons/Nordzy-dark -f`

### Hibernate

<https://linuxconfig.org/how-to-restore-hibernation-on-fedora-35>

## System Upgrade

[How to Upgrade to Fedora 38 from Fedora 37 - LinuxCapable](https://www.linuxcapable.com/how-to-upgrade-fedora-release/)

1. `sudo dnf upgrade --refresh`
2. `sudo dnf autoremove`
3. `sudo dnf install dnf-plugin-system-upgrade` (just in case, usually "nothing to do" 'cause already installed)
4. `sudo dnf system-upgrade download --releasever=38`
5. `sudo dnf system-upgrade reboot`
6. `sudo dnf system-upgrade clean`
7. broken symlinks: `sudo symlinks -r /usr | grep dangling` to view, then `sudo find /usr -type l -xtype l -delete` to delete them

### Kernels

View/edit the amount of past kernels to keep:

`installonly_limit` in `/etc/dnf/dnf.conf`

#### List all installed kernels

To get the index number of all the installed kernels:

`sudo grubby --info=ALL | grep -E "^kernel|^index"`

Change the default kernel using index:

`sudo grubby --set-default-index=1`

Verify:

`sudo grubby --default-title`

### Network Manager

Config files are stored here: `/etc/NetworkManager/system-connections/`

## Software

### DNF

Useful commands: [25 Useful DNF Command Examples For Package Management In Linux](https://www.rootusers.com/25-useful-dnf-command-examples-for-package-management-in-linux/)

Use `dnf search pkg` to check availability of any package.

Exclude packages when updating: `sudo dnf update --exclude=kernel\*`

Listing installed packages: `dnf list installed`

With filters: `dnf list installed "x*"`

### Firefox Dev. Edt.

Fixing the `*.desktop` file / missing icon:

Location should be: `/usr/share/applications/`

NB: could also be located in `/usr/local/share/applications/` - move it if it is!

Inside the `*.desktop` file:

```
[Desktop Entry]
Version=1.0
Encoding=UTF-8
Name=Firefox Developer Edition
Comment=Firefox Developer Edition
#Exec=/usr/bin/firefox-developer
Exec=/opt/firefox-developer/firefox-bin %u
Icon=firefox-developer-icon.svg
Type=Application
StartupNotify=true
Keywords=firefox;dev;browser
Categories=Internet
StartupWMClass=firefox-aurora
```

Note the last line specifically, that did make *all* the difference (10/2023, Fedora 38).

### KeeWeb

RPM: from official GitHub

### Postman

Update must be done manually:

[fedy/install.sh at master · rpmfusion-infra/fedy](https://github.com/rpmfusion-infra/fedy/blob/master/plugins/postman.plugin/install.sh)

1. Download latest Release from <https://www.postman.com/downloads/>
2. Extract: `tar -xzf postman-linux-x64.tar.gz`
3. Replace current version: `sudo cp -r Postman /opt/`

### Teams

→ Pkg from Fedora repos works fine so far

Screen Sharing *does not* work in the desktop app, only from the browser with this flag enabled (Chromiums): `WebRTC PipeWire support`

**10/2023**: works in Brave without any specific config.

See:

- <https://techcommunity.microsoft.com/t5/discussions/screen-share-on-linux-build-on-wayland-does-not-work/m-p/2278031>
- <https://docs.microsoft.com/en-us/answers/questions/94638/window-sharing-in-teams-linux.html>
- <https://docs.microsoft.com/en-us/answers/questions/408938/sharing-tray-emptyblankblack-in-teams-on-ubuntu-21.html>

### OneDrive

<https://github.com/abraunegg/onedrive>

CLI, GUI as an additional pkg
