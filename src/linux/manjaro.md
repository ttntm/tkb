# Manjaro

## Setup

- nvidia:
  - <https://forum.manjaro.org/t/how-to-set-up-nvidia-drivers-correctly/50649/8>
  - <https://forum.manjaro.org/t/i-dont-think-my-system-uses-nvidia-at-all-when-using-bumblebee/92916/5>
  - this seems to be the way to go → <https://forum.manjaro.org/t/>guide-install-and-configure-optimus-manager-for-hybrid-gpu-setups-intel-nvidia/92196
- setup timeshift backups
- hugo: <https://www.ostechnix.com/safely-ignore-package-upgraded-arch-linux/>
- windows drives: <https://forum.manjaro.org/t/trouble-mounting-ntfs-drives-not-appearing-in-run-mount/55761/37>
- protonvpn: <https://forum.manjaro.org/t/howto-installation-and-usage-of-protonvpn/115476>
- soulseek client - done
- keeweb → needed `gnome-keyring` in order to run as normal (local) application vs. chrome web app
- improve the mount scripts for SG-XT → <https://unix.stackexchange.com/questions/281282/how-to-prevent-sda-sdb-changes-between-boots>

USB → use [https://www.balena.io/etcher](https://www.balena.io/etcher/)

Without Dual Boot:

- [https://www.happycoders.eu/de/devops/manjaro-tutorial-installation-manjaro-linux-dell-xps-15-9570](https://www.happycoders.eu/de/devops/manjaro-tutorial-installation-manjaro-linux-dell-xps-15-9570/)

Dual Boot Windows Setup:

- <https://forum.manjaro.org/t/howto-dual-boot-manjaro-windows-10-step-by-step/52668>
- system time → <https://forum.manjaro.org/t/howto-get-your-time-timezone-right-using-manjaro-windows-dual-boot/89359/3>
- <https://gist.github.com/MeirBon/c99ae4e9e40f7e542361f646e4f637f5>
  - for XPS 15 9560 though
  - according to <https://wiki.archlinux.org/index.php/Dell_XPS_15_9560> that also matches my machine
    - Before installing it is necessary to modify some UEFI Settings. They
      can be accessed by pressing the F2 key repeatedly when booting.
      - Change the SATA Mode from the default "RAID" to "AHCI". This will allow Linux to detect the NVME SSD. If dual booting with an existing Windows installation, Windows will not boot after the change but [this can be fixed without a reinstallation](https://triplescomputers.com/blog/uncategorized/solution-switch-windows-10-from-raidide-to-ahci-operation/).
      - Change Fastboot to "Thorough" in "POST Behaviour". This prevents intermittent boot failures.
      - Disable secure boot to allow Linux to boot.
  - GeForce 1050
    - <https://forum.manjaro.org/t/dell-xps-9560-intel-nvidia-bumblebee-drivers-install-issue/44545/5>
    - <https://wiki.manjaro.org/Configure_Graphics_Cards#How_to_check_the_driver>
- <https://wiki.archlinux.org/index.php/Dell_XPS_15_9570>
- KDE Plasma "Blur" Desktop effect → "some KDE desktop effects depend on having appropriate OpenGL support" (see: <https://www.linuxquestions.org/questions/slackware-14/some-kde-desktop-effects-suddenly-stopped-working-4175481738/>)
- KDE icon themes → `~/.local/share/icons`
  - globals/defaults → `/usr/share/icons`
- Useful apps → <https://wiki.archlinux.org/index.php/List_of_applications>

## Shell

trying `bash` → `zsh` atm; see: <https://wiki.archlinux.org/index.php/Zsh>

good guide: <https://medium.com/tech-notes-and-geek-stuff/install-zsh-on-arch-linux-manjaro-and-make-it-your-default-shell-b0098b756a7a>

<https://bbs.archlinux.org/viewtopic.php?id=166433&p=2>

## Rounded corner glitch

- <https://psifidotos.blogspot.com/2020/04/article-rounded-plasma-and-qt-csds.html?m=1>
    - <https://www.pling.com/p/1352754>
- <https://forum.manjaro.org/t/mcmojave-window-borders/149735>

## Locale errors

...no idea why that happened

- <https://forum.manjaro.org/t/default-locale-not-set-properly/64163/28>
- <https://forum.manjaro.org/t/localize-manjaro/42946>

## PacMan

List orphaned packages: `pacman -Qqtd`

Remove orphaned packages: `pacman -Qqd | pacman -Rsu -`
