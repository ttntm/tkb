# Setups

A history of my setups over the years.

Combine screenshots: [onlineconverter](https://www.onlineconverter.com/merge-images)

## Current Devices

- [Lenovo ThinkPad P14s](./thinkpad.md)
- [Dell Precision M3800](./precision.md)

## Past Devices

- [HP EliteBook 850 G7](./elitebook.md)
- [Dell XPS 15](./xps.md)
