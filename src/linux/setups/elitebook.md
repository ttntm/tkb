# HP EliteBook 850 G7

Status: former work notebook, stopped using this device in autumn 2023

OS: [Fedora](../fedora.md)

## Summer 2021

![HP EliteBook Setup screenshot from Summer 2021](images/fedora_elitebook.jpg)
