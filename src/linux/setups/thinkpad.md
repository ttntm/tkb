# Lenovo ThinkPad P14s

Status: current work notebook

OS: [Fedora](../fedora.md)

## Configuration

[Spring '24](https://codeberg.org/ttntm/fedora-spring24)

### Power Management

TLP didn't do too much, trying [power-profiles-daemon](https://gitlab.freedesktop.org/upower/power-profiles-daemon) as of 01/2024.

#### Charging Thresholds

Set them in `/sys/class/power_supply/BAT0`:

- `charge_control_start_threshold`
- `charge_control_end_threshold`

Work via `thinkpad_acpi`

See: [github/tpacpi-bat](https://github.com/teleshoes/tpacpi-bat)

## Screenshots

### Spring 2024

![ThinkPad setup screenshot from Spring 2024](images/fedora_thinkpad_spring24.jpg)

### Autumn/Winter 2023

![ThinkPad setup screenshot from Autumn/Winter 2023](images/fedora_thinkpad.jpg)
