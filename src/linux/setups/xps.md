# Dell XPS 15 9570

Status: former personal notebook, stopped using this device in summer 2021

OS: [Manjaro](../manjaro.md)

## Spring 2021

![Dell XPS Setup screenshot from Spring 2021](images/manjaro_xps_3.jpg)

## Autumn/Winter 2020

![Dell XPS Setup screenshot from Autumn/Winter 2020](images/manjaro_xps_2.jpg)

## Spring 2020

![Dell XPS Setup screenshot from Spring 2020](images/manjaro_xps_1.png)
