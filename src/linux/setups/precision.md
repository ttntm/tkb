# Dell Precision M3800

Status: current personal notebook

OS: [Manjaro](../manjaro.md)

## Winter 2023

![Dell Precision Setup screenshot from Winter 2023](images/manjaro_precision_2.png)

## Summer 2021

![Dell Precision Setup screenshot from Summer 2021](images/manjaro_precision_1.png)
