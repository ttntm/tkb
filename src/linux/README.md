# Linux

Linux topic index page.

## Pages

- [Fedora](./fedora.md)
- [Gnome](./gnome.md)
- [Manjaro](./manjaro.md)
- [Setups](./setups/index.html)

## Resources

- [Linux Crisis Tools](https://www.brendangregg.com/blog/2024-03-24/linux-crisis-tools.html)
- [explainshell.com](https://explainshell.com/)
- [linuxsurvival.com](https://linuxsurvival.com/) - An interactive Linux tutorial
- [Use Linux Unified Key Setup](https://opensource.com/article/21/3/encryption-luks)
- [victoria.dev/tags/linux/](https://victoria.dev/tags/linux/)

### Cheat Sheets

![Linux Cheat Sheet](images/linux-cheatsheet.jpg)

#### File System Infographic

![The Linux File System](images/linux_fs.png)

#### Observability Tools

![Linux Observability Tools](images/linux_observability_tools.png)

## Customization

- [FirefoxCSS Store](https://trickypr.github.io/FirefoxCSS-Store.github.io/)
- [Nerdfonts](https://www.nerdfonts.com/font-downloads)
- [The Ultimate Oldschool PC Font Pack](https://int10h.org/oldschool-pc-fonts/)
- [Themer](https://themer.dev)

### 256 Colors

List of 256 colors for Xterm prompt (console). Contains displayed color, Xterm Name, Xterm Number, HEX, RGB and HSL codes.

[256 colors cheat sheet](https://www.ditig.com/publications/256-colors-cheat-sheet)
