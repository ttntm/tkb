# TypeScript

Notes on TypeScript.

## Notes

`Object.keys(o).map(...)` errors: see [Use Generics](https://stackoverflow.com/a/61400965) for some suggestions regarding the use of generics.

## Resources

- [Cheat Sheet](https://devhints.io/typescript)
- [Deep Dive](https://basarat.gitbook.io/typescript)
- [The TSConfig Cheat Sheet](https://www.totaltypescript.com/tsconfig-cheat-sheet)

### Templates

- [typescript-node](https://github.com/ttntm/typescript-node/)
