# HTML

## Details/Summary

Fully animate such elements: [How to fully animate the details HTML element with only CSS and no JavaScript](https://dev.to/jgustavoas/solved-how-to-fully-animate-the-details-html-element-with-only-css-and-no-javascript-55ek)

## Favicon/Icon

```html
<link rel="icon" href="/favicon.ico" sizes="32x32">
<link rel="icon" href="/icon.svg" type="image/svg+xml">
<link rel="apple-touch-icon" href="/apple-touch-icon.png"><!-- 180×180 -->
```

If PWA:

```html
<link rel="manifest" href="/manifest.webmanifest">
```

```json
// manifest.webmanifest
{
  "icons": [
    { "src": "/icon-192.png", "type": "image/png", "sizes": "192x192" },
    { "src": "/icon-mask.png", "type": "image/png", "sizes": "512x512", "purpose": "maskable" },
    { "src": "/icon-512.png", "type": "image/png", "sizes": "512x512" }
  ]
}
```

`maskable`: safe zone is a 409×409 circle

## noopener/noreferrer

...is obsolete, browsers handle that for us today.

Source: MDN via [Target=_blank implies rel=noopener](https://www.stefanjudis.com/today-i-learned/target-blank-implies-rel-noopener/)

**BUT**: [External links and target="_blank"](https://ttntm.me/blog/external-links-and-target-blank/)

## Number Inputs

Don't use `type="number"` use `type="text" inputmode="numeric"` instead.

`number` silently restricts input, `inputmode` provides the number keyboard on mobile without any of the downsides.

## Page Search

re: a11y of elements without text labels - [hidden="until-found"](https://schepp.dev/posts/rethinking-find-in-page-accessibility-making-hidden-text-work-for-everyone/#the-solution%3A-hidden%3D%22until-found%22)

## When to Inline SVGs

Inline:

- Small number of SVG(s) with < 5k file size each – inline them in HTML. Compressed gzip/brotli each will be around 1k. Any small number multiplied by 1k is better than same number of additional HTTP requests
- Any number of SVG(s), but we do need to use CSS to change a SVG property or animate some SVG property - the only viable option is inline svg

Don't inline:

- Any number of big SVG(s) > 5k and we do not need to access SVG properties via CSS or JS
- Huge number of small SVG(s) < 5k - Make SVG sprite

If we need SVG(s) for backgrounds but they are < than 5k each:

- `.bg { background: url('data:image/svg+xml;utf8,<svg ...> ... </svg>'); }`

If we need SVG(s) for backgrounds but they are > than 5k each:

- `.bg { background: url('images/file.svg'); }`

Maybe: SVG sprites as background might be an option too

Source: [so/57665241](https://stackoverflow.com/a/57665241)
