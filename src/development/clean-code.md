# Clean Code & Design Smells

## Reading

- [A Practical Introduction to Clean Coding](https://www.freecodecamp.org/news/clean-coding-for-beginners/)
- [clean-code-javascript](https://github.com/ryanmcdermott/clean-code-javascript)
- [Google JavaScript Style Guide](https://google.github.io/styleguide/jsguide.html)

## No Arguments Signal Side-Causes

Whenever you see a function with no arguments, one of two things are true: Either it always returns exactly the same value, or it's getting its inputs from elsewhere (ie. it has side-causes).

For example, this function must always, always return the same integer (or it has side-causes):

`public Int foo() {}`

## No Return Value Signals Side-Effects

And whenever you see a function with no return value, then either it has side-effects, or there was no point calling it:

`public void foo(...) {...}`

According to that function signature, there is absolutely no reason to call this function. It doesn't give you anything. The only reason to call it is for the magical side-effects it promises it will silently cause.

[Which Programming Languages Are Functional?](https://blog.jenkster.com/2015/12/which-programming-languages-are-functional.html)

## Reduce Nesting

![reduce nesting](images/js-reduce-nesting.jpg)
