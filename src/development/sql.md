# SQL

SQL notes.

## Joins

A quick reminder image:

![SQL Joins](images/SQL_Joins.png)

## OVER / PARTITION BY

[SQL PARTITION BY Clause overview](https://www.sqlshack.com/sql-partition-by-clause-overview/)

## Performance and Optimization

[SQL Optimization: a comprehensive developer's guide](https://www.eversql.com/sql-optimization-a-comprehensive-developers-guide/): a couple of "standard practices that you can implement to improve performance" when working with SQL.

### Use Cases

#### Applying DISTINCT to a Single Column

Use case: customers that bought items from a range of products in one or more orders in the last 90 days.

Example data:

| OrderId | Email | ProductId |
|--------|--------|--------|
| 123 | bob@xmpl.com | A1 |
| 123 | bob@xmpl.com | B4 |
| 456 | bob@xmpl.com | E1 |

We want the email address `bob@xmpl.com` to appear only once in our query results. All other data is irrelevant, because we already know that every row in the source table qualifies for the above criteria based on a previous query.

```sql
SELECT *
FROM (
  SELECT
    Email
    , ROW_NUMBER() OVER(PARTITION BY Email ORDER BY OrderId DESC) rn
  FROM xmpl
) AS tmp
WHERE tmp.rn = 1
```

Source: <https://stackoverflow.com/a/5021739>

#### Joining Tables With Nothing in Common

**Use case example**: table A contains customers, table B contains coupon codes that haven't been assigned to customers yet.

```sql
SELECT
  CU.Id
  ,CU.EmailAddress
  ,CC.Code AS CouponCode
FROM (
  SELECT
    Id
    ,EmailAddress
    ,row_number() OVER (ORDER BY Id) AS Rn
  FROM Customers
) AS CU
INNER JOIN
(
  SELECT
    Code
    ,row_number() OVER (ORDER BY Code) AS Rn
  FROM CouponCode
  WHERE CustomerId IS NULL OR CustomerId = ''
) AS CC
ON CU.Rn = CC.Rn
```

Source: <https://stackoverflow.com/a/17621974>
