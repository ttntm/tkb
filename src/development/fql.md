# FQL

Fauna Query Language

## Links

- [awesome-fauna](https://github.com/fauna-labs/awesome-fauna)
- [sql2fql](https://github.com/fauna-labs/sql2fql)
- <https://forums.fauna.com/t/multi-document-upsert/488/13>
- <https://forums.fauna.com/t/sql-group-by-having-counterpart-in-faunadb/434>

## Pagination

- <https://support.fauna.com/hc/en-us/articles/4950324676507>

## Schema Migration

- [fauna-schema-migrate](https://github.com/fauna-labs/fauna-schema-migrate)
