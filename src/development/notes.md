# Notes

Miscellaneous dev notes that I haven't found a better place for yet.

## Email

- [DMARC/SPF records for unused domains](https://www.kuketz-blog.de/warum-du-fuer-ungenutzte-domains-spf-und-dmarc-records-setzen-solltest/)
- [Understanding SPF, DKIM, and DMARC: A Simple Guide](https://github.com/nicanorflavier/spf-dkim-dmarc-simplified)

## QR Codes

...can be generated easily (+ offline) with LibreOffice.

Docs: [QR and Barcode](https://help.libreoffice.org/latest/en-US/text/shared/01/qrcode.html)

## RegEx

URL incl. `localhost`:

`/^https?:\/\/\w+(\.\w+)*(:[0-9]+)?(\/.*)?$/g`
