# CRDTs

CRDT stands for "Conflict-free Replicated Data Type".

It’s a kind of data structure that can be stored on different computers (peers). Each peer can update its own state instantly, without a network request to check with other peers. Peers may have different states at different points in time, but are guaranteed to eventually converge on a single agreed-upon state. That makes CRDTs great for building rich collaborative apps, like Google Docs and Figma - without requiring a central server to sync changes.

Broadly, there are two kinds of CRDTs: state-based and operation-based.1 State-based CRDTs transmit their full state between peers, and a new state is obtained by merging all the states together. Operation-based CRDTs transmit only the actions that users take, which can be used to calculate a new state. They’re also known as _CvRDTs_ ("Cv" standing for "convergent") and _CmRDTs_ ("Cm" standing for "commutative"), respectively, although "state-based" and "operation-based" might be the preferred terms.

Source and a lot more details: [An Interactive Intro to CRDTs](https://jakelazaroff.com/words/an-interactive-intro-to-crdts/)

Example application: [Building a Collaborative Pixel Art Editor with CRDTs](https://jakelazaroff.com/words/building-a-collaborative-pixel-art-editor-with-crdts/)

## Reading

- [Designing Data Structures for Collaborative Apps](https://mattweidner.com/2022/02/10/collaborative-data-design.html)
- [An introduction to Conflict-Free Replicated Data Types](https://lars.hupel.info/topics/crdt/01-intro/)
- [A CRDT Primer Part I: Defanging Order Theory](https://blog.jtfmumm.com/2015/11/17/crdt-primer-1-defanging-order-theory/)
- [A CRDT Primer Part II: Convergent CRDTs](https://blog.jtfmumm.com/2015/11/24/crdt-primer-2-convergent-crdts/)
