# Software Architecture

## Pages

- [CRDT](./crdt.md)
- [REST APIs](./rest.md)
- [RPC](./rpc.md)
- [Web Applications](./web-applications.md)

## Architectural Styles

A handy cheat sheet from ByteByteGo:

![Architectural styles cheat sheet](images/architectural_styles.jpg)

### Responsibility-Driven Design

![Responsibility-Driven Design](images/rdd.png)

Source: [3 Steps to Solve Most Design Problems | How to Find Abstractions Effectively](https://khalilstemmler.com/letters/3-steps-to-solve-most-design-problems/)

## Non-Functional Requirements

![Functional vs non functional requirements](images/functional-vs-non-functional-requirements.png)

Source: [Non-Functional Requirements (with Examples)](https://khalilstemmler.com/articles/object-oriented/analysis/non-functional-requirements/)

## Resources

- [Client-Side Architecture Basics](https://khalilstemmler.com/articles/client-side-architecture/introduction/)
- [How Complex Systems Fail](https://how.complexsystems.fail)
- [Software Architecture (in another personal wiki)](https://yoan-thirion.gitbook.io/knowledge-base/software-architecture)
- [Software Architecture Guide](https://martinfowler.com/architecture/)
  - A guide to material on martinfowler.com about software architecture
- [wiki @ khalilstemmler.com](https://khalilstemmler.com/wiki/)
