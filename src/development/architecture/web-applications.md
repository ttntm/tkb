# Web Applications

## Networking

Options to send events from the server to the client.

Source: [WebSockets vs Server-Sent-Events vs Long-Polling vs WebRTC vs WebTransport](https://rxdb.info/articles/websockets-sse-polling-webrtc-webtransport.html)

### Long-Polling

The first "hack" to enable a server-client messaging method that can be used in browsers over HTTP, it emulates server push communications with normal XHR requests.

Long polling establishes a connection to the server that remains open until new data is available. Once the server has new information, it sends the response to the client, and the connection is closed. Immediately after receiving the server's response, the client initiates a new request, and the process repeats.

### Web Sockets

A full-duplex communication channel over a single, long-lived connection between the client and server.

Used for data exchange without the overhead of HTTP request-response cycles, facilitating real-time data transfer for applications like live chat, gaming, or financial trading platforms.

Ideal for scenarios that require low latency and high-frequency updates.

### Server-Sent Events (SSE)

A standard way to push server updates to the client over HTTP.

Designed exclusively for one-way communication from server to client, making them ideal for any situation where the client needs to be updated in real time without sending data to the server, i.e. live news feeds.

Can be compared to a single HTTP request where the backend does not send the whole body at once, but instead keeps the connection open and trickles the answer by sending a single line each time an event has to be sent to the client.

### WebTransport

A cutting-edge API designed for efficient, low-latency communication between web clients and servers.

Status: Working Draft

It leverages the [HTTP/3 QUIC protocol](https://en.wikipedia.org/wiki/HTTP/3) to enable a variety of data transfer capabilities, such as sending data over multiple streams, in both reliable and unreliable manners, and even allowing data to be sent out of order.

Can be used for applications requiring high-performance networking, such as real-time gaming, live-streaming, and collaborative platforms.

### WebRTC

An open-source project and API standard that enables real-time communication (RTC) capabilities directly within web browsers and mobile applications without the need for complex server infrastructure or the installation of additional plugins. Supports peer-to-peer connections for streaming audio, video, and data exchange between browsers.

But: needs a signaling-server which would run over websockets, SSE or WebTransport. This defeats the purpose of using WebRTC as a replacement for these technologies.

## When CRUD and MVC isn't Enough

From: [Knowing When CRUD & MVC Isn't Enough | Enterprise Node.js + TypeScript](https://khalilstemmler.com/articles/enterprise-typescript-nodejs/when-crud-mvc-isnt-enough/)

> Know when we need a domain model

When we realize that we're getting a lot of business logic and rules that don't quite fit within the CRUD confines, it might be a sign to move from a transaction script to domain modeling.

> Frameworks aren't a silver bullet

Frameworks help us become productive a lot faster, and they help by making architectural decisions for us, but we still need to learn how to do domain modeling (or something else fancy) if our CRUD app becomes sufficiently complex.

Frameworks can't always make that decision for us.

> DDD is an introduction to some really interesting architectures

DDD is a gateway to some really interesting event-driven architectures like CQRS (command query response segregation) and Event Sourcing.
