# Development Resources & Tools

- [AutoRegex](https://www.autoregex.xyz) (English to RegEx)
- [Beekeeper Studio](https://www.beekeeperstudio.io)
- [Dan's Tools](https://www.danstools.com)
- [Describe and Caption Images](https://astica.ai/vision/describe-images/)
- [DevDocs](https://devdocs.io)
- [free-for.dev](https://free-for.dev) (a list of software and other offerings with free developer tiers)
- [Free for life](https://free.wdh.gg) (another list including products and services that are completely free)
- [HTTP response status codes](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status)
- [iHateRegEx](https://ihateregex.io)
- [IT-Tools](https://it-tools.tech)
- [JetBrains Guide](https://www.jetbrains.com/guide/)
- [JWT Debugger](https://jwt.io/#debugger-io)
- [Naming things](https://classnames.paulrobertlloyd.com)
- [NimbleText](https://nimbletext.com/live) (Text manipulation and code generation)
- [Omatsuri](https://omatsuri.app) (Collection of everyday tools)
- [Quick Reference](https://quickref.me) (Cheat Sheets)
- [Quickchart QR Code API](https://quickchart.io/documentation/qr-codes/)
- [quicktype](https://quicktype.io) (Types from JSON)
- [Saltify](https://www.saltify.io) (Secret sharing)
- [web-utils](https://u.matdoes.dev)

## Documentation

- [C4 Model for Software Architecture](https://c4model.com) (Context, Containers, Components and Code)
- [ChartDB](https://chartdb.io) (A free and open source, database design editor)
- [dbdiagram.io](https://dbdiagram.io) (A free, simple tool to draw ER diagrams by just writing code)
- [Excalidraw](https://excalidraw.com)
- [GIFcap](https://gifcap.dev) (In-Browser GIF capture, client-side only)
- [Markdown To PDF](https://md2pdf.netlify.app)
- [PlantText](https://www.planttext.com)
- [PlantUML Editor](https://plantuml-editor.kkeisuke.com)
- [The Hitchhiker’s Guide to PlantUML](https://crashedmind.github.io/PlantUMLHitchhikersGuide/)

## Graphics

- [Betty's Graphics](https://bettysgraphics.neocities.org)
- [carbon](https://carbon.now.sh) (Images of code)
- [favicon.io](https://favicon.io)
- [Pexels](https://www.pexels.com) (stock photos)
- [Phosphor Icons](https://phosphoricons.com)
- [Pic Smaller](https://picsmaller.com) ([OSS](https://github.com/joye61/pic-smaller) image compression, supports batch processing)
- [Screely](https://screely.com) (Browser mockup screenshots)
- [Squoosh](https://squoosh.app) (Image compression)
- [SVG Encoder / Converter](https://svgencode.com)
- [Tabler Icons](https://tabler.io/icons)
- [unDraw](https://undraw.co)

## WebDev

See: [webdev/resources](./webdev/resources.md)
