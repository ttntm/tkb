# Web Development Resources

- [a11yphant](https://a11yphant.com) (teaches web accessibility)
- [An introduction to HTTP](https://www.freecodecamp.org/news/http-and-everything-you-need-to-know-about-it)
- [Butterick's Practical Typography](https://practicaltypography.com)
- [Codrops/Demos](https://tympanus.net/codrops/demos/) (500+ animations, layouts, UI styles, etc.)
- [HTML Entities](https://www.compart.com/en/unicode/html) (unicode icons/symbols)
- [Information Architecture](images/information-architecture.jpg) (infographic)
- [Laws of UX](https://lawsofux.com)
- [MDN Curriculum](https://developer.mozilla.org/en-US/curriculum/)
- [Microfrontend Template](https://georgwittberger.github.io/microfrontend-template/)
- [micro-frontend.dev 2.0](https://microfrontend.dev)
- [The A11Y Project Checklist](https://www.a11yproject.com/checklist/)
- [User Agents](./user-agents.md)
- [viewports.fyi](https://viewports.fyi)
- [Web Dev Toolkit](https://gomakethings.com/toolkit/) (A collection of boilerplates, helper functions, and libraries)

## Colors & Design

- [Color Shades Generator](https://javisperez.github.io/tailwindcolorshades/) (Colors for Tailwind CSS)
- [ColorSpace](https://mycolor.space) (Enter a color code, get a color palette)
- [Colour Contrast Checker](https://colourcontrast.cc)
- [fffuel](https://www.fffuel.co) (color tools and SVG generators)
- [Happy Hues](https://www.happyhues.co) (Curated colors)
- [i want hue](https://medialab.github.io/iwanthue/)
- [Maskable.app](https://maskable.app) (preview your maskable PWA icons)
- [Nord Color Generator](https://nordhealth.design/color-generator/)
- [Picture Palette](https://picture-palette.web.app) (get colors from pictures)
- [Poolors](https://poolors.com) (color combos least/most used by designers)
- [Randoma11y](https://randoma11y.com) (Randomly generated accessible color palettes)
- [Super design tools](https://superdesigner.co/tools) (SVG backgrounds, shapes, gradients etc.)
- [Theme Machine](https://tools.keithjgrant.com/theme-machine/)
- [UIColors](https://uicolors.app/create) (Colors for Tailwind CSS)
- [Visual Center](https://javier.xyz/visual-center) (Find the visual center of images)
- [WebAIM Contrast Checker](https://webaim.org/resources/contrastchecker/)

## CSS

- [CSS Layout Generator](https://layout.bradwoods.io)
- [CSS-only Shapes](https://css-shape.com)
- [CSS Pattern](https://css-pattern.com)
- [Project Wallace](https://www.projectwallace.com) (CSS analyzer)
- [Tailwind CSS buttons](https://buttons.ibelick.com)

## Fonts

- [Font Pairing Guide](https://connary.com/pairing.html)
- [google-webfonts-helper](https://gwfh.mranftl.com/fonts)
- [Modern Font Stacks](https://modernfontstacks.com)
- [UNCUT.wtf](https://uncut.wtf) (OSS fonts)

## Performance & Testing

- [bundlejs](https://bundlejs.com) (A quick npm package size checker)
- [CSP Evaluator](https://csp-evaluator.withgoogle.com)
- [dead link checker](https://www.deadlinkchecker.com/website-dead-link-checker.asp)
- [Front-End Checklist](https://frontendchecklist.io)
- [Meta Tags Toolkit](https://metatags.io)
- [Mozilla Observatory](https://observatory.mozilla.org)
- [Nu Html Checker](https://validator.w3.org/nu/) (HTML checking)
- [opengraph.xyz](https://www.opengraph.xyz) (Preview and Generate Open Graph Meta Tags)
- [PageSpeed Insights](https://pagespeed.web.dev)
- [Pingdom Website Speed Test](https://tools.pingdom.com)
- [securityheaders.com](https://securityheaders.com)
- [SSL Server Test](https://www.ssllabs.com/ssltest/)
- [W3C Feed Validation Service](https://validator.w3.org/feed/)
- [Web Check](https://web-check.xyz)
- [webhint](https://webhint.io)
- [WebPageTest - Lighthouse](https://www.webpagetest.org/lighthouse)
- [Yellow Lab Tools](https://yellowlab.tools) (Page speed audit)
