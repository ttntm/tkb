# Astro

## Documentation

- **Project Structure** guide: [project-structure](https://docs.astro.build/en/core-concepts/project-structure/)
- **Config** file documentation: [config](https://astro.build/config)
- **Integrations**: [integrations](https://astro.build/integrations/)
- **Pages**: [astro-pages](https://docs.astro.build/en/core-concepts/astro-pages/)
- **UI Components**: [framework-components](https://docs.astro.build/en/core-concepts/framework-components/)
- **State Management**: [sharing-state](https://docs.astro.build/en/core-concepts/sharing-state/)
  - **Nanostores Docs**: [nanostores](https://github.com/nanostores/nanostores)
- **RSS**: [guides/rss](https://docs.astro.build/en/guides/rss/)
- **Images**: [guides/images](https://docs.astro.build/en/guides/images/)
- **i18n**: [recipes/i18n](https://docs.astro.build/en/recipes/i18n/)
  - +[astro-i18n-aut](https://github.com/jlarmstrongiv/astro-i18n-aut)

## CMS Integration

- [Contentful](./cms/contentful.md)
- [Kontent.ai](./cms/kontentai.md)
- [Storyblok](./cms/storyblok.md)

## Notes

Per default, all components - regardless of framework used - turns to 0 JS HTML output.

Astro Components can have **props** and **slots** like framework components:

- [component props](https://docs.astro.build/en/core-concepts/astro-components/#component-props)
- [slots](https://docs.astro.build/en/core-concepts/astro-components/#slots)

So-called [Component Islands](https://docs.astro.build/en/concepts/islands/) **make individual parts of the site interactive**, using a directive like `client:load` on the component level.

**Templating**: uses JSX-like syntax, see [JSX-like expressions](https://docs.astro.build/en/core-concepts/astro-components/#jsx-like-expressions)

### Client Directives

Controls how and when framework JS is loaded and/or gets to be interactive.

See: [client directives](https://docs.astro.build/en/reference/directives-reference/#client-directives)

Keep `client:only` in mind - it skips server-side rendering altogether.

### Content - Tags

See: [tags tutorial](https://docs.astro.build/en/tutorial/5-astro-api/2/)

We need to get the content, go through the metadata and make that a list on the content list page that can be used for filtering or b, good old - individual - taxonomy pages.

The big question: can this be done with 1 CMS query, or does it require multiple requests? - YES

### CSS & Styling

Component-based, i.e. include `<style>` tag in components; automatically *scoped* only.

**Global styles**: use `<style is:global>` and pack that into a separate component.

**Fonts**: use [Fontsource](https://docs.astro.build/en/guides/fonts/#using-fontsource), or work with locally sourced (manually maintained) files; i.e. proprietary fonts unavailable via said package.

### Deployment on GitHub Pages

Make sure to include a `.nojekyll` file (due to the `_astro` folder).

Also keep in mind that the `base` option should be set accordingly, i.e. repo name. Otherwise, imports will be broken.

### Environment Variables

Kept in the usual files.

See: [guides/environment-variables](https://docs.astro.build/en/guides/environment-variables/)

### Images

Config for CMS image processing:

```js
image: {
  remotePatterns: [{
    protocol: 'https',
    hostname: '**.example.com'
  }]
}
```

- Store local images in `./src/assets/`
- Import the `Image` or `Picture` component from `astro:assets`
- Import the picture itself

For CMS images: the OOTB components need to have both `width` and `height` specified explicitly. There will be an error otherwise.

For framework components: use `getImage()`, but keep in mind that _it will only work on the server side_. Works for static components, but any interactivity (i.e. `v-if`) will break the frontend JS.

Docs: [guides/images](https://docs.astro.build/en/guides/images/)

### JSX

Using `switch()` statements:

```jsx
{(() => {
  switch (true) {
    case elType === 'value':
      return <Component />

    default:
      return <div style="display: none">unmatched element</div>
  }
})()}
```

### Pages & Routing

Works based on the files in the `/pages` directory.

Layout should be based on the files in the `/layouts` directory.

`*.js` and `*.ts` files in the `/pages` directory can be used as API routes.

Plain `*.html` files are (almost) left as they come - see: [HTML components](https://docs.astro.build/en/core-concepts/astro-components/#html-components); can be used for static content and/or help migration efforts.

Navigating between pages can be done with normal `<a href="...">` tags; no need to maintain a router config either.

**Dynamic routes** (i.e. blog posts) are based on a page component like `[post].astro` (= [named parameter](https://docs.astro.build/en/core-concepts/routing/#named-parameters)); must use and export the `getStaticPaths()` function (see [modern-astro-blog/.../blog/[slug].astro](https://github.com/stefkudla/modern-astro-blog/blob/master/src/pages/blog/%5Bslug%5D.astro) for an example of how this can be done using a headless CMS).

Named parameter must conform to the requirements of `getStaticPaths()`; see: [getStaticPaths()](https://docs.astro.build/en/reference/api-reference/#getstaticpaths).

Pages have a **global** `fetch()` **function** (ala nuxt): [data fetching](https://docs.astro.build/en/guides/data-fetching/); provides top-level await and can be used to fetch data from the CMS. NB: will only fetch data **once at build time**. Is **also available in framework components**.

### Performance & Optimizations

Prefetching: [guides/prefetch](https://docs.astro.build/en/guides/prefetch/)

### State Management

See docs link above.

Separate stores per use case, each one its own `*.ts` file.

### Search

custom search form that navigates to `/search` with an url param containing the query

try the default pagefind search result component first, re-style as necessary

custom `resultTemplate` via [resultList.js#L31](https://github.com/CloudCannon/pagefind/blob/971186cdd062995c14d2cc2bc711b09c850d086f/pagefind_ui/modular/components/resultList.js#L31)

- [pagefind.app](https://pagefind.app)
  - [Adding a Simple Search to an Astro Blog](https://trost.codes/posts/adding-simple-search-to-an-astro-blog/)
  - [astro-pagefind](https://github.com/shishkin/astro-pagefind)

### Sitemap

Works OOTB using the [sitemap integration](https://docs.astro.build/en/guides/integrations-guide/sitemap/#configuration)

### Typescript

See: [guides/typescript](https://docs.astro.build/en/guides/typescript/)

Typed component props: [component props](https://docs.astro.build/en/guides/typescript/#component-props)
