# iOS/Safari Quirks & Workarounds

## Targeting Safari

### Desktop

```css
@supports (hanging-punctuation: first)
  and (font: -apple-system-body)
  and (-webkit-appearance: none) {}
```

### iOS

```css
@supports (-webkit-touch-callout: none) {}
```

## Known Issues

`:hover` states:

- <https://dev.webonomic.nl/fixing-the-iphone-css-hover-problem-on-ios>

`backdrop-blur` not working consistently:

- workaround: set background opacity to 100% for the affected UI elements

`input` with `type="date"`:

- must have `height`/`max-height` set explicitly
- `width` is also a good idea

`<summary>` styling - customizations that work in other browsers (i.e. icon color) looked broken; the following styles restored the default look:

```css
summary {
  list-style: none !important;
}

summary::marker,
summary::-webkit-details-marker,
summary ::marker,
summary ::-webkit-details-marker {
  display: none !important;
}

summary h2 {
  display: block !important;
}
```

Viewport units: [Fixing the iOS Toolbar Overlap Issue with CSS Viewport Units](https://opus.ing/posts/fixing-ios-safaris-menu-bar-overlap-css-viewport-units)

Code that assumes that toolbars are expanded, just to be safe:

```css
.header {
  min-height: calc(100vh - 6rem);
}

@supports (height: 100svh) {
  .header {
    min-height: calc(100svh - 6rem);
  }
}
```
