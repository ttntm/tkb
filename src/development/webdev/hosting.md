# Hosting

Hosting provider options.

## Europe

### Germany

- [ALL-INKL.COM](https://all-inkl.com/webhosting/paketvergleich/)
- [Uberspace](https://uberspace.de)

### Sweden

- [statichost.eu](https://www.statichost.eu)

## UK

- [Mythic Beasts](https://www.mythic-beasts.com)

## US

- [Cloudflare](https://www.cloudflare.com)
- [Fly.io](https://fly.io)
  - Transforms containers into micro-VMs
