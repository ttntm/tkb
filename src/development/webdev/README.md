# Web Development

Web Development topic index page.

## Pages

- [Astro](./astro.md)
- [CMS](./cms/index.html)
- [Eleventy](./eleventy.md)
- [Hosting](./hosting.md)
- [iOS/Safari](./ios-safari.md)
- [Resources](./resources.md)
- [User Agents](./user-agents.md)

## Project Setup

Interesting: [A CSS project boilerplate](https://piccalil.li/blog/a-css-project-boilerplate/) for 2024

## Request Headers

![Request Headers](../images/request-headers.png)
