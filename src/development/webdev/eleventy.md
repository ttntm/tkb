# Eleventy

Notes on 11ty.

Docs: [Eleventy Documentation](https://www.11ty.dev/docs/)

## Environment Variables

Docs: [docs/environment-vars](https://www.11ty.dev/docs/environment-vars/)

[Eleventy Supplied](https://www.11ty.dev/docs/environment-vars/#eleventy-supplied) environment variables are an easy way to handle differences between dev/prod in config files, i.e. turning off expensive transformations (like `htmlmin`) during local development.

## v3 Notes

When using the image plugin, caching should be taken care of. [Cloudflare](../devops/cloudflare.md) Pages seems to support caching, but only for Eleventy's `.cache` directory.

Another approach would be using GitHub's actions cache: [Deploy an Eleventy site to Cloudflare Pages efficiently with GitHub's action cache](https://jonas.brusman.se/deploy-eleventy-to-cloudflare-with-githubs-action-cache/)
