# Contentful

## Astro Integration

KB Page: [Astro](../astro.md)

- <https://docs.astro.build/en/guides/cms/contentful/#overview>

## Rendering Content

HTML Renderer: <https://github.com/contentful/rich-text/tree/master/packages/rich-text-html-renderer>

### Embedded/inlined blocks

- <https://www.contentful.com/blog/rendering-linked-assets-entries-in-contentful/>
- <https://stackoverflow.com/questions/75333820/how-do-i-render-embedded-code-blocks-from-contentful-with-prism-in-astro>
- Using actual Astro components?
  - <https://stackoverflow.com/questions/73382889/how-can-i-render-a-astro-component-to-a-html-string> => <https://github.com/withastro/roadmap/issues/533>

### Issues

- `p` tags inside `li` elements:
  - A workaround using RegEx: <https://github.com/contentful/rich-text/issues/126#issuecomment-827745138>

## Linked Entries (i.e tags)

See <https://github.com/contentful/contentful.js/issues/1932#issuecomment-1594215661> for how to handle types for them (esp. `withoutUnresolvableLinks`).

## JavaScript Lib

### Docs

[Getting Started with Contentful and JavaScript](https://www.contentful.com/developers/docs/javascript/tutorials/using-js-cda-sdk/)

### Notes

> By default, Contentful resolves one level of linked entries or assets.

Meaning: getting blog posts includes the "linked entries" of type "Tag" in the `getEntries()` response.

Conclusion: **keep relationships at 1 level for simple sites**.
