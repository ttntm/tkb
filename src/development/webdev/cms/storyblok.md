# Storyblok

[Tutorials | Storyblok](https://www.storyblok.com/tutorials)

## Astro Integration

KB Page: [Astro](../astro.md)

- <https://www.storyblok.com/mp/announcing-storyblok-astro>
- <https://docs.astro.build/en/guides/data-fetching/#example-storyblok-api>
- <https://www.storyblok.com/tp/add-a-headless-cms-to-astro-in-5-minutes>

## Dynamic HTML

- <https://tailwindcss.com/docs/typography-plugin>

## Blog

- <https://www.storyblok.com/tp/create-and-render-blog-articles-in-storyblok-and-nuxt>
- <https://vuedose.tips/guides/how-we-built-our-blog-as-a-full-static-site-with-nuxt-storyblok-and-tailwindcss>

### Tags & Search

- <https://vuedose.tips/tags-and-search-functionality-in-nuxt-using-storyblok-api>
    - <https://www.storyblok.com/docs/api/content-delivery#core-resources/tags/tags>

## Rich Text Rendering

Included as `import { renderRichText } from '@storyblok/astro'`

### Resolve Components in Richt Text

Use `setComponentResolver()` - see: <https://github.com/storyblok/storyblok-js-client#method-storybloksetcomponentresolver>

"Post" component should import all allowed components and chose based on a switch statement

**Define *one-time* use:**

See: <https://github.com/storyblok/storyblok-js#rendering-rich-text>

You can also set a **c**ustom Schema and component resolver **only once** by passing the options as the second parameter to `renderRichText` function:

```jsx
renderRichText(blok.richTextField, {
  resolver: (component, blok) => {
    switch (component) {
      case "my-custom-component":
        return `<div class="my-component-class">${blok.text}</div>`
        break
      default:
        return `Component ${component} not found`
    }
  }
})
```

→ try this asap to see if that's an approach that makes sense

*NB: keep in mind to break out of tailwind's `prose` class if sticking with it*
