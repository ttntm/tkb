# Kontent.ai

[Astro](../astro.md) example: https://docs.astro.build/en/guides/cms/kontent-ai/

SDK: [kontent-ai/delivery-sdk-js](https://github.com/kontent-ai/delivery-sdk-js)

Rich Text resolution via SDK: [resolving-rich-text-elements](https://github.com/kontent-ai/delivery-sdk-js?tab=readme-ov-file#resolving-rich-text-elements)

Rich Text resolution via Portable Text:

1. [Make your rich text portable](https://kontent.ai/blog/make-your-rich-text-portable/)
    - [kontent-ai/rich-text-resolver-js](https://github.com/kontent-ai/rich-text-resolver-js)
2. [astro-portabletext](https://github.com/theisel/astro-portabletext/tree/main/astro-portabletext)

Doing it in Astro: [Resolving Rich Text in Astro and Next.js](https://www.luminary.com/blog/resolving-rich-text-in-astro-and-next-js)

### API

[Kontent.ai APIs](https://www.postman.com/kontent-ai/workspace/kontent-ai-apis/) (Postman collection)

### Nesting Blocks

[Rich Text - Adding content items](https://kontent.ai/learn/docs/content-items/rich-text#a-adding-content-items)

Landing page example: [Build landing pages](https://kontent.ai/learn/model/creator-s-role-in-content-modeling/build-landing-pages)
