# CMS

Content management systems.

## Pages

- [Contentful](./contentful.md)
- [Kontent.ai](./kontentai.md)
- [Storyblok](./storyblok.md)

## Research

### Contentful

Still quite powerful, generous free tier.

See: [Contentful](./contentful.md)

Rendering stuff in [Astro](../astro.md) works a lot like [Storyblok](./storyblok.md) - that's great.

### Decap CMS

ex-Netlify CMS

open source + free

runs on own site, pushes to git for changes

<https://github.com/decaporg/decap-cms>

Interesting: [DecapCMS and GitLab integration without Netlify](https://predragtasevski.com/blog/decapcms-and-gitlab-integration-without-netlify)

### Kirby

PHP, but no DB

Can do headless with SSG

Needs server to run on - even `localhost` would be ok for static builds!

#### Cost

99€ per site

See: <https://getkirby.com/buy>

#### Hosting/VPS

<https://forum.getkirby.com/t/list-of-good-hosting-providers-for-kirby/2121/4>

See [Hosting](../hosting.md) for other options.

#### Starter: Headless + Nuxt

- [cacao-kit-frontend](https://github.com/johannschopplich/cacao-kit-frontend)
- [cacao-kit-backend](https://github.com/johannschopplich/cacao-kit-backend)

### Publii

> Building, styling and creating content for your site all happens offline on your desktop, whether you're using Windows, Mac or Linux. The complete site files are rendered and uploaded to your server automatically at the click of a button.

[Website](https://getpublii.com)

### Tina CMS

good ol' forestry

<https://tina.io/docs/>

"Basic" is still free forever: <https://tina.io/pricing/>

[Astro](../astro.md) support:

- <https://docs.astro.build/en/guides/cms/tina-cms/>
- <https://github.com/tombennet/astro-tina-starter/tree/main>
