# Apex

Apex is a strongly typed, object-oriented programming language that's optimized to run in the Salesforce multitenant architecture. Apex allows developers to automate complex backend business processes, and gets compiled into Java bytecode.

The Apex language is optimized to interact with Salesforce data and is tightly integrated with the Salesforce persistence layer. Apex provides SOQL (Salesforce Object Query Language), similar to SQL, for executing queries and DML (Data Manipulation Language) statements for performing database operations with the objects you created earlier.

Apex is opinionated, tightly coupled, and optimized to work with business apps that work well with Salesforce. It fails quickly at compile time if any references are invalid.

[Governors and limits](https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_gov_limits.htm)

## Testing

[Testing best practices](https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_testing_best_practices.htm)
