# LWCs

## Docs

- [Component record context](https://developer.salesforce.com/docs/platform/lwc/guide/use-record-context.html)
- [Decorators](https://developer.salesforce.com/docs/platform/lwc/guide/reference-decorators.html)
- [Targets](https://developer.salesforce.com/docs/platform/lwc/guide/reference-configuration-tags.html)

## Lightning Component Library

The reference for both Aura and Lightning web components and how to use them is found at [docs/component-library/overview/components](https://developer.salesforce.com/docs/component-library/overview/components).

You can view the library through your org's instance, too, at `http://<MyDomainName>.lightning.force.com/docs/component-library`. By viewing the library through your instance, you see only the correct version for your org. And, as you create your own custom components, they appear in the library too.

## Lightning Data Service (LDS)

Access data and metadata from Salesforce via Lightning Data Service. Base Lightning components that work with data are built on LDS. Customize your own components to take advantage of LDS caching, change-tracking, performance, and more.

## Lightning Locker

Lightning web components that belong to one namespace are secure from components in a different namespace through Security with Lightning Locker. Lightning Locker also promotes best practices that improve the supportability of your code by only allowing access to supported APIs and eliminating access to nonpublished framework internals.

## Use Apex in LWCs

- [Base components](https://developer.salesforce.com/docs/atlas.en-us.lightning.meta/lightning/lightning_overview.htm)
- Recipes: [lwc-recipes](https://github.com/trailheadapps/lwc-recipes)

To allow an [Apex](./apex.md) method to be used in an LWC, you annotate it with the `@AuraEnabled` annotation. You can import the `@AuraEnabled` method to LWC as a function.

When used with the `@wire` decorator, the component retrieves data using the Apex method.
