# SFDC

Notes on SFDC.

## Pages

- [Apex](./apex.md)
- [LWCs](./lwc.md)

## Dev Setup

- [Code samples and SDKs](https://developer.salesforce.com/code-samples-and-sdks)
- [Visual Studio Code Salesforce Extension Pack](https://marketplace.visualstudio.com/items?itemName=salesforce.salesforcedx-vscode)

### API

[postman-salesforce-apis](https://github.com/forcedotcom/postman-salesforce-apis)

### Java Setup

See: [Java setup](https://developer.salesforce.com/tools/vscode/en/vscode-desktop/java-setup)

`$JAVA_HOME/bin/java -version`

```json
{
    "salesforcedx-vscode-apex.java.home": "/usr/lib/jvm/java-21-openjdk-21.0.3.0.9-1.fc40.x86_64"
}
```

### Metadata

In VS Code, using the Org Browser.

Retrieved metadata goes to: `./force-app/main/default/objects`

Using the CLI:

`sf project retrieve start --metadata CustomApplication:Dreamhouse CustomTab:House__c "Layout:House__c-House Layout"`

### Order of Execution

Diagram: [Order of Execution Overview](../images/Salesforce-Order-Of-Execution-Diagram.png)

## Known Issues

### Get Consumer Key and Secret for Existing User

consumer key / secret = client id / secret

1. Log in using the user
2. Switch to classic UI
3. Go to _Create > Apps_
4. Select a Connected App
5. Section _API_, click button "Manage Consumer Details"

### MCCO Stopped Working

**Symptoms** include but are (probably?) not limited to:

- Emails re: "Salesforce Marketing Cloud Connect - Action Required to Prevent Service Interruption"
- SF Data Entry Events used in Journey Builder are not receiving data anymore (existing and newly created)
- Debug logs show "FireJBTrigger" events, but no data arrives in SFMC

#### Troubleshooting

- [Troubleshooting Journey Builder Integration with Debug Logs](https://sfmarketing.cloud/2019/10/09/troubleshooting-journey-builder-integration-with-debug-logs/)
- [Troubleshooting Marketing Cloud Connect](https://sfmarketing.cloud/2019/07/31/troubleshooting-marketing-cloud-connect/)

#### Fix

[Remove Configuration Settings in Salesforce for MCCO](https://help.salesforce.com/s/articleView?id=000387432&type=1)

Prerequisites:

- Username/Password for Marketing Cloud API User used by the Integration
- User is a System Administrator in Salesforce

**Add the 'Configurations' Tab**

1. Click Setup.
2. Follow the steps based on your UI:
    - Salesforce Classic: Under the "Create" category, select Tabs.
    - Salesforce Lightning: Under the "User Interface" category, select Tabs.
3. Click New.
4. Choose "Configurations" from the Object drop-down menu.
5. Pick any Tab style.
6. Click Next | Next | Save.

**Delete Stored Configuration**

1. Click the Plus symbol on the tab bar.
2. Click on the Configurations link.
3. Choose All under the "View" drop-down, and then click Go.
4. Delete the existing Configuration Object.

**Reconfigure Marketing Cloud Connect**:

1. Click the Marketing Cloud Tab.
2. Enter the Marketing Cloud API Username and Password.
3. Select values for Send Types, Target Audience, Exclusion Audience, Support Ticket Recipient, and Tracking Preferences.
4. Click Marketing Cloud Tab once more, re-integrate individual Users as needed.

> Error:
> IO Exception: Unauthorized endpoint, please check Setup->Security->Remote site settings. endpoint = https://customer--et4ae5.vf.force.com/services/Soap/m/42.0

Check and add the missing URL to "Setup" > "Security" > "Remote site settings" if this error shows up when re-connecting to SFMC:

- Login to Salesforce
- Navigate to Setup > Security > Remote Site Settings
- Search for "MetadataAPI"
- Click Edit
- Replace the existing URL with the URL contained in the error message
- Click Save

NB: This was the _only_ approach that ended up working. The other approach described in [Troubleshooting Marketing Cloud Connect](https://sfmarketing.cloud/2019/07/31/troubleshooting-marketing-cloud-connect/) (running code via "Execute anonymous") did not work out.
