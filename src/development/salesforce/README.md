# Salesforce

SF development topic index page.

## Pages

- [SFDC](./sfdc/index.html)
- [SFMC](./sfmc/index.html)

## Maintenance of Certifications

Trailhead: [Maintain Your Salesforce Certifications](https://trailhead.salesforce.com/content/learn/trails/maintain-your-salesforce-certifications)
