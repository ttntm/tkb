# Known Issues

## AMPscript

### Inline `RedirectTo(Concat())` + `Base64Encode()`

Broken Code

```html
<a href="%%=RedirectTo(concat(
    'example.com/unsubscribe?param1=',
    _SubscriberKey,
    '&param2=',
    Base64Encode(emailaddr, 'UTF-16')
  ))=%%">
  link
</a>
```

Fixed Code

```html
%%[
  set @email = emailaddr
  set @email64 = Base64Encode(@email, "UTF-16")
  set @unsub1 = concat("https://example.com/unsubscribe?param1=", _subscriberkey)
  set @unsub2 = concat("&param2=", @email64)
  set @unsub = concat(@unsub1, @unsub2)
]%%

<a href="%%=RedirectTo(@unsub)=%%">link</a>
```

## Email Delivery

### Bounce Management Quirks

From SF Support:

> If a subscriber bounces on an email send and a BCC'd address interacts with the email, it's possible for a false bounce to be marked for the subscriber.


> If the system attempts delivery X times and the message bounces each time, the subscriber's status is set to Bounced. If the email is delivered during the X attempts and the subscriber opens the email, the subscriber's status is set to Active. Also, a bounced subscriber can find an old email from you in their inbox and open it, which returns their status to Active.


> Since our status logging system only registers when a contact goes between DELETED, HELD, NORMAL and UNSUB, I'm not seeing this contact go between a possible bounce state and then back to active from an open.

### Hidden Unsubscribes

Unsubscribe reasons like

- Unsusbcribed from the Subscription Center
- Unsubscribed by Salesforce Marketing Cloud RMM service based on subscriber Leave/Reply email
- Spam Complaint

all refer to interactions on mail server level, i.e. "Unsubscribe" in the email headers of Gmail, Yahoo etc.

SF explanation: [Marketing Cloud 'List-Unsubscribe' header](https://help.salesforce.com/s/articleView?language=en_US&id=000332579&type=1&utm_source=Asksalesforce&utm_medium=social_organic)

NB: it's necessary to catch such cases and write them back to CRM if MC Connect is in use.

Possible solution: use a webhook: [Event Notification Service](https://developer.salesforce.com/docs/marketing/marketing-cloud/guide/ens-get-started.html)

### Recipient Email Address - Email Studio vs. Journey Builder

- **Email Studio** sends use the Contact's stored email address, regardless of different values in a Data Extension
- **Journeys** use the Data Extensions respective email address if available and configured in the journey settings

## Journey Builder

### Salesforce Data Entry Event Not Triggering

Happened with a new Business Unit where MC Connect was already working (pulling data from SFDC into the sync DEs).

**Solution**: the BU needs to be enabled (= checkbox checked) in the SFDC MCCO settings:

![mcco bu settings](../images/mcco_bu_settings.png)

Careful when working with new BUs: the main integration user (root BU, most likely from initial setup; listed on the main page of MC Connect admin) must be mapped to the new BU (in SFMC) first for it to show up in the MC Connect admin panel.

**If MCCO seems broken for other reasons**, proceed as described at "MCCO Stopped Working" in [SFDC](../sfdc/index.html).

### Validation Errors

> The event key used in the event expression does not match the journey's entry event key.

Diesen Fehler bekommt man, wenn man Journey mit einer DE + Automation als Entry Event definiert, speichert und danach - aber noch vor Aktivierung der Journey = bei pausierter Automation - die DE in einen anderen Ordner verschiebt.

Es dürfte ein Fehler im Mapping von den Journey Settings -> Email Address from Entry Source... sein.

**Lösung**: das Event Löschen, Journey speichern und nochmal konfigurieren.

### Decision Split - NULL Values

Decision splits need to explicitly handle empty/null values, otherwise routing will most likely be broken for empty fields in journey DEs.

Example - Head, "FR" exclusion:

![jb nulls](../images/jb_nulls.png)

## SSJS

### Long Integers / Scientific Notation

SSJS can turn them into scientific notation, i.e. 1e21

Can be prevented using `Number.toFixed()`:

```js
var id = 1e21
var idFixed = id.toFixed()
```

NB: works up to 1e21 = 1000000000000000000000 (22 characters)

Additional info: [How to avoid scientific notation for large numbers in JavaScript?](https://stackoverflow.com/questions/1685680/how-to-avoid-scientific-notation-for-large-numbers-in-javascript)

### Missing `String.prototype.trim()`

SSJS is stuck at ECMA 3, trim() is ECMA 5.

We can use the following snippet to make trim() available:

```js
/**
 * Backport String.prototype.trim()
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/trim
 */
String.prototype.trim = function () {
  return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '')
}
```

**Source**: [How to use trim() function in SSJS](https://salesforce.stackexchange.com/a/285511)

### Log Unsub Event Errors

LUE errors when "jobId" can not be found via listId lookup.

That happens DESPITE this functional description; "JobId" defaults to the most recent send the subscriber was a part of:

>The Job Context is defined by the JobID, ListID and BatchID parameters. These values are used to determine which Job the UnsubEvent is tracked against. The subscriber is also unsubscribed from the List that the Job was sent to. You don't need to supply all three values. The system looks up any missing values using the following rules:
>1. If the JobID is supplied, we can lookup a missing ListID and/or BatchID.
>2. If the ListID is supplied, we can lookup a missing JobID and/or BatchID. (a, If the JobID is missing, we use the most recent JobID that the subscriber was sent to. b, This may not be the Job that the Subscriber is acting upon.)
>3. If only the BatchID is supplied, we cannot lookup the remaining information and the job context is not defined.
>
> If the job context cannot be established because you did not supply any of these parameters or only supplied the BatchID, the UnsubEvent is not created. The subscriber is also Master Unsubscribed from the system instead of being unsubscribed from a particular list. Remove the ListID to address the All Subscribers list in an account.

**Solution**: Add a fallback to the WSProxy call that uses the [default](https://developer.salesforce.com/docs/marketing/marketing-cloud/guide/ssjs_subscriberUnsubscribe.html) `unsubscribe()` to prevent unprocessed unsubscribes.

### Switch with multiple case statements in the same branch

Don't do that, it will lead to the branch listed first getting ignored by the SSJS engine:

```js
switch(o.key) {
  case 'a':
  case 'b':
    // a/b code
    break

  case 'c':
    // c code
    break
}
```

Use a `default` instead:

```js
switch(o.key) {
  case 'c':
    // c code
    break

  default:
    // a/b code
}
```

### `Script.Util.HttpRequest()`

Returns something in .NET CLR (Common Language Runtime) format.

The response needs to be processed like this to make it usable by JS:

```js
/**
 * `Script.Util.HttpRequest` returns the response in a .NET (?) CLR format.
 * This function makes the response usable for JS.
 * @returns {object | undefined}
 */
function processResponse(res) {
  if (!res || !res.content) {
    return undefined
  }

  var resContent = String(res.content)

  return Platform.Function.ParseJSON(resContent)
}
```

### Use of Common Language Runtime (CLR) is not allowed

Test with the following code:

```js
  Platform.Load("core","1.1.1");

  var endpoint = "https://www.google.com";
  var req = new Script.Util.HttpRequest(endpoint);
    req.emptyContentHandling = 0;
    req.retries = 2;
    req.continueOnError = true;
    req.method = "GET";
    req.setHeader("Cache-Control","no-cache");
  var resp = req.send();

  try {
    Platform.Response.Write("Return header Stringify: " + Stringify(resp.headers) + "<br>");
    Platform.Response.Write("Return header: " + resp.headers["Server"] + "<br>");

    var resultJSON = Platform.Function.ParseJSON(String(resp.headers["server"]));

    Platform.Response.Write("Return header ParseJSON: " + Stringify(resultJSON));
  } catch (e) {
    Write(Stringify(e));
  }
```

If that returns the same error: a support case is necessary!

**Source**: [SSJS: read response headers from Script.Util.HttpRequest object in SFMC](https://salesforce.stackexchange.com/questions/325286/ssjs-read-response-headers-from-script-util-httprequest-object-in-sfmc)

### Missing Array.prototype.indexOf()

```js
/**
 * Polyfill Array.prototype.indexOf()
 */
if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = (function (Obj, max, min) {
    'use strict'
    return function indexOf(member, fromIndex) {
      if (this === null || this === undefined) {
        throw TypeError('Array.prototype.indexOf called on null or undefined')
      }

      var that = new Obj(this)
      var len = that.length
      var i = min(fromIndex || 0, len)

      if (i < 0) {
        i = max(0, len + i)
      } else if (i >= len) {
        return -1
      }

      if (member === undefined) {
        for (; i !== len; ++i) {
          if (that[i] === undefined && i in that) {
            return i // undefined
          }
        }
      } else if (member !== member) {
        for (; i !== len; ++i) {
          if (that[i] !== that[i]) {
            return i // NaN
          }
        }
      } else {
        for (; i !== len; ++i) {
          if (that[i] === member) {
            return i // all else
          }
        }
      }

      return -1 // if the value was not found, then return -1
    }
  }(Object, Math.max, Math.min))
}
```
