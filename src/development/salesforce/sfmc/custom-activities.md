# Custom Activities

Notes on Custom Activities in Journey Builder.

## Architecture

Contrary to what many examples out there might suggest, it's not necessary to develop a full blown dynamic web application (i.e. Node with Express) - static web apps can also be used as Custom Activities, for example when used as forwarding services to SSJS resources that do the "heavy lifting".

![sfmc ca architecture](../images/sfmc_ca_architecture.png)

## Lessons Learned

### Entry Event - Data Extension Columns

You can use undocumented 'requestSchema' Postmonger event to retrieve schema of event data source: [Custom Activity: Get the name of the Data Extension you are working with](https://salesforce.stackexchange.com/a/221888)

```js
var connection = new Postmonger.Session();
connection.on('requestedSchema', requestedSchemaHandler);
connection.trigger('requestSchema');

// ...

function requestedSchemaHandler(data) {
  if (data && data.schema && data.schema.length > 0) {
    console.log(data.schema)
  } else {
    console.error('Error obtaining schema')
  }
}
```

NB: this code can even be run in the browser console (with the context switched to the CA iFrame).

#### Example schema

```js
[
  {
    "key": "Event.DEV_OrderNotification.OrderStatusId",
    "name": "OrderStatusId",
    "type": "Text",
    "length": 30,
    "default": null,
    "isNullable": null,
    "isPrimaryKey": true
  }
]
```

### Entry Event - Data Binding

DO NOT use strings that escape the column name - it doesn't work.

Bad:

```js
"Campagne": "{{Contact.Attribute." + eventDefinitionKey + ".\"Campagne\"}}"
```

Good:

```js
"Campagne": "{{Event." + eventDefinitionKey + ". Campagne}}"
```

Even better:

```js
const getLookupBinding = (a,b) => `{{Event.${a}.${b}}}`
```

### Entry Event - Definition Key

Use the postmonger event `requestInteraction` to get the whole event definition.

The resulting data does not include the linked DE's column names, refer to `requestSchema` for that.

Special use case: SF data entry events - seems like the DO include the fields selected in the event (as per [th3mkn/sfmc-servicecloud-customsplit#L64](https://github.com/th3mkn/sfmc-servicecloud-customsplit/blob/master/public/customActivity.js#L64) ff.).
