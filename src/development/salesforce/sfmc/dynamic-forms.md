# Dynamic Forms

![sfmc dynforms](../images/sfmc_dynforms.png)

## Type Definitions

```js
/**
 * @typedef FormField
 * @type {object}
 * @property {string} name Field name
 * @property {('text'|'email'|'phone'|'checkbox'|'radio'|'select')} type Controls rendering and maps to the `type` attribute of the respective `<input>` element
 * @property {string} displayName Field label
 * @property {string?} placeholder Placeholder text; will use 'displayName' as fallback
 * @property {boolean?} required Marks the field as 'required'
 * @property {boolean?} readonly Marks the field as 'readonly' ('text'|'email'|'phone')
 * @property {string?} className CSS class/es to pass to the rendered `<input>` element
 * @property {Options[]?} options Defines available options for checkbox/radio/select fields
 */

/**
 * @typedef Options
 * @type {object}
 * @property {string} value Checkbox/Select: controls data mapping (target `[key]` of mapped state object). Radio: value for `[key]` specified via FormField.name
 * @property {string} text Displayed option text
 * @property {boolean?} required _Checkbox only!_ Marks checkbox as required (Radio/Select: control via parent FormField.required)
 */
```
