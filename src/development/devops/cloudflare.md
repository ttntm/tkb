# Cloudflare

## Bots

See: [Concepts > Bots](https://developers.cloudflare.com/bots/concepts/bot/)

### Bot Fight Mode vs WAF

BFM runs before the WAF; this is what allows enterprise customers to make firewall rules based on feedback from the bot protection.

BUT: even if a bot solved the challenge, if you have a firewall rule blocking them, they would see a block page instead of your website.

Source: [CF Community](https://community.cloudflare.com/t/bot-fight-mode-vs-waf-block/545336/4)

### Verified Bots

See: [Verified Bots](https://radar.cloudflare.com/traffic/verified-bots)

### WAF - Whitelisting User Agents

Create a custom rule.

1. Field = User Agent
2. Operator = equals
3. Value = bot UA, i.e. `EchoFeed fetch bot; https://echofeed.app`
4. "Then take action..." = Skip
5. "WAF components to skip" = select all
6. "Place at" = First

## Pages

(Soft) limit: 100

### Cache / CDN

Setup: need a **cache rule** to work properly, even if the rule is simply set up as a catch-all.

Rule configuration:

- Hostname: equals `my-domain.com`
- Cache eligibility: "Eligible for cache"
- Edge TTL: "Use cache-control header if present, use default Cloudflare caching behavior if not"
- Browser TTL: "Respect origin TTL" (see below re: cache-control header)
- Cache key:
  - Cache deception armor: active
  - Ignore query string: active

The website should also send a cache-control header; it can be used to customize how CF handles caching.

Purge on demand: [Purge cache](https://developers.cloudflare.com/cache/how-to/purge-cache/)

### Custom Domains

Using apex domains is only possible when changing name servers to CF.

Otherwise, subdomains can be used with CNAME records.

### Deployment

Deploy static HTML: use `exit 0` as a build command as described in [Deploy with Cloudflare Pages](https://developers.cloudflare.com/pages/framework-guides/deploy-anything/#deploy-with-cloudflare-pages)

#### Build Caching

Docs: [Build caching (beta)](https://developers.cloudflare.com/pages/configuration/build-caching/)

#### Deploy From Self-Hosted GitLab

- [Deploy to Cloudflare Pages with Gitlab CI](https://techtitbits.com/posts/cloudflare-pages-with-gitlab-ci/)
- [How to Deploy from GitLab to Cloudflare Pages](https://www.tderflinger.com/en/how-to-deploy-from-gitlab-to-cloudflare-pages)

#### Deploy Hooks

Trigger deployments from other services: [Deploy Hooks](https://developers.cloudflare.com/pages/configuration/deploy-hooks/)

#### Skip Builds

Use `[CI Skip]`, `[CI-Skip]`, `[Skip CI]`, `[Skip-CI]` or `[CF-Pages-Skip]` flag as a prefix in the commit message.

Docs: [​​Skipping a specific build via a commit message](https://developers.cloudflare.com/pages/configuration/git-integration/#skipping-a-specific-build-via-a-commit-message)

### Migrate from *

- [Netlify](https://developers.cloudflare.com/pages/migrations/migrating-from-netlify/)

### Redirects

Use [bulk redirects](https://developers.cloudflare.com/rules/url-forwarding/bulk-redirects/) to hide the default `*.pages.dev` domain that is created automatically.

## Traffic Sequence

How CF processes incoming traffic:

![CF Traffic Sequence diagram](./images/cf_traffic_sequence.jpg)

## Workers

Environment variables that are secrets: [Secrets](https://developers.cloudflare.com/workers/configuration/secrets/)

Local development: use a `.dev.vars` file.

### Cron Triggers

Docs: [Setting Cron Triggers](https://developers.cloudflare.com/workers/examples/cron-trigger/)

### Using RPC

Blog post: [We've added JavaScript-native RPC to Cloudflare Workers](https://blog.cloudflare.com/javascript-native-rpc/)

Docs: [Runtime APIs > Remote-procedure call (RPC)](https://developers.cloudflare.com/workers/runtime-apis/rpc/)

#### Reducing Round Trips

If you know that a call will return a value containing a stub, and all you want to do with it is invoke a method on that stub, you can _skip awaiting it_:

```js
let foo = stub.foo()
let baz = foo.bar.baz()
let corge = await baz.qux[3].corge()
```

What actually happens is, when the return value is passed over [RPC](../architecture/rpc.md), all class instances are replaced with RPC stubs. The stub, when called, makes a new RPC back to the server, where it calls the method on the original User object that was created there. The RPC stub is a special object called a [Proxy](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy). It implements a "wildcard method", meaning that it appears to have an infinite number of methods of every possible name. When you try to call a method, the name you called is sent to the server. If the original object has no such method, an exception is thrown.

RPC methods do not return normal promises. Instead, they return special RPC promises. These objects are [custom thenables](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise#thenables), which means you can use them in all the ways you'd use a regular Promise, like awaiting it or calling `.then()` on it.

But an RPC promise is more than just a thenable. It is also a proxy. Like an RPC stub, it has a wildcard property. You can use this to express speculative RPC calls on the eventual result, before it has actually resolved. These speculative calls will be sent to the server immediately, so that they can begin executing as soon as the first RPC has finished there, before the result has actually made its way back over the network to the client.

This feature is also known as "Promise Pipelining". Although it isn't explicitly a security feature, it is commonly provided by object-capability RPC systems like [Cap'n Proto](https://en.wikipedia.org/wiki/Cap%27n_Proto).
