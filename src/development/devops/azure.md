# Azure

## DevOps

Defining environment variables in pipelines:

```yml
- task: taskName
  env:
    SVC_USERNAME: $(svc_username)
    SVC_ACCESS_KEY: $(svc_key)
```

Mirroring repos from GitLab: [Creating an Azure DevOps mirror for Gitlab repository](https://noob-programmer-journal.blogspot.com/2021/01/creating-azure-devops-mirror-for-gitlab.html)

### Pipelines

Caching: [Pipeline caching](https://learn.microsoft.com/en-us/azure/devops/pipelines/release/caching)

Predefined variables: [Use predefined variables](https://learn.microsoft.com/en-us/azure/devops/pipelines/build/variables)

**For Node.js functions**:

- Deployment package must be _the whole cwd_, not just the `/dist` folder
- Manually syncing triggers might be necessary - include a quick bash script using `curl` (see: [Trigger syncing](https://learn.microsoft.com/en-us/azure/azure-functions/functions-deployment-technologies?tabs=linux#trigger-syncing))

#### Using Webhooks to Trigger a Pipeline

Get the pipeline's URL from the browser, i.e. `https://dev.azure.com/{organization}/{project}/_build?definitionId=1`.

Edit this template, adding the values obtained from the browser: `https://dev.azure.com/{organization}/{project}/_apis/build/builds?definitionId={definitionId}&api-version=6.0`

Create a personal access token for your account.

To trigger the pipeline: send a POST request with a basic auth header that includes your email address and the PAT, i.e. `me@example.com:aCooltoken123etc`, encoded in Base64.

NB: the PAT needs the scope `vso.build_execute`.

## Functions

- [Node.js developer guide](https://learn.microsoft.com/en-us/azure/azure-functions/functions-reference-node)
- [Quickstart: Create a JavaScript function](https://learn.microsoft.com/en-us/azure/azure-functions/create-first-function-vs-code-node)
- [Quickstart: Create a TypeScript function](https://learn.microsoft.com/en-us/azure/azure-functions/create-first-function-vs-code-typescript)
- [Best practices](https://learn.microsoft.com/en-us/azure/azure-functions/functions-best-practices)

An URL to check deployments: `https://FUNC_NAME.scm.azurewebsites.net/api/deployments`

### Specifics

- [Environment variables](https://learn.microsoft.com/en-us/azure/azure-functions/functions-reference-node?tabs=javascript%2Clinux%2Cazure-cli&pivots=nodejs-model-v4#environment-variables)
  - [Local settings file](https://learn.microsoft.com/en-us/azure/azure-functions/functions-develop-local#local-settings-file)
- [host.json reference](https://learn.microsoft.com/en-us/azure/azure-functions/functions-host-json)
- [Version 4 of the Node.js programming model](https://techcommunity.microsoft.com/t5/apps-on-azure-blog/azure-functions-version-4-of-the-node-js-programming-model-is-in/ba-p/3773541)
- [Continuous delivery by using GitHub Actions](https://learn.microsoft.com/en-us/azure/azure-functions/functions-how-to-github-actions)

## SWA

Application configuration: [Configure Azure Static Web Apps](https://learn.microsoft.com/en-us/azure/static-web-apps/configuration)

Build configuration: [Build configuration for Azure Static Web Apps](https://learn.microsoft.com/en-us/azure/static-web-apps/build-configuration)

### Custom Domains

Redirecting `www` to an apex domain can cause issues with the certificate.

Solution: make sure that the `www` subdomain is also added to the SWA's custom domains config.

More info: [Static Web App: www subdomain not working with automatic SSL certificate](https://learn.microsoft.com/en-us/answers/questions/1353926/static-web-app-www-subdomain-not-working-with-auto)

### Specifics

- [Deploy GitLab repositories on Azure Static Web Apps](https://learn.microsoft.com/en-us/azure/static-web-apps/gitlab)
