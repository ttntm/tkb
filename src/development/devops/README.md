# DevOps

> DevOps is a set of practices, tools, and a cultural philosophy for software delivery. DevOps automates and bridges processes between software development and IT teams.

ToDo: [The DevOps engineer's handbook](https://octopus.com/devops/)

## Pages

- [Azure](./azure.md)
- [Cloudflare](./cloudflare.md)
- [Kubernetes](./kubernetes.md)
