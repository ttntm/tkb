# JavaScript Resources & Links

- [Design Patterns](https://designpatternsgame.com) (as a game)
- [Eloquent JavaScript](https://eloquentjavascript.net)
- [HTML DOM](https://phuoc.ng/collection/html-dom/)
- [JavaScript cheatsheet](https://quickref.me/javascript)
- [JSHint](https://jshint.com/docs/)
- [Mnemonist](https://yomguithereal.github.io/mnemonist/) (data structures for the JavaScript language)
- [Modules](https://exploringjs.com/es6/ch_modules.html)
  - [Modules @ MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules)
- [Packages](https://exploringjs.com/nodejs-shell-scripting/ch_packages.html)
- [The Modern JavaScript Tutorial](https://javascript.info)
- [You Don't Know JS Yet](https://github.com/getify/You-Dont-Know-JS)
