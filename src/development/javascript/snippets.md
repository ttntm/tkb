# JavaScript Snippets

## Check for Broken Links

Can be used in a browser's DevTools (hence the `$$`):

```js
$$('[href]:not([href=""])').forEach(anchor => {
  console.info({
    anchor,
    href: anchor.href
  })

  fetch(anchor.href, {
    method: 'HEAD'
  })
})
```

## Date - Timestamp

Will produce `YYYYMMDD`:

```js
function getTimestamp() {
  const d = new Date()
  return d.toISOString().split('T')[0].replaceAll('-', '')
}
```

## Date - YMD

Will produce `2024-10-20`:

```js
function getDateYMD() {
  const d = new Date().toLocaleString('en-US', {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit'
  }).split('/')

  return `${d[2]}-${d[0]}-${d[1]}`
}
```

## Debugging Objects

...will avoid `[Object]`s in deeply nested trees.

```js
console.dir(obj, { depth: Infinity })
```

## Page Visibility API

Can be used to handle changes in `document` (i.e. browser tab) visibility:

```js
document.addEventListener('visibilitychange', () => {
  if (document.hidden) {
    document.title = 'X unread messages'
  } else {
    document.title = 'Inbox'
  }
})
```

## Protect Global Browser Networking APIs

```js
function protect() {
  const protected = {
    configurable: false,
    writable: false
  }

  window.XMLHttpRequest.prototype.addEventListener = () => {}
  Object.defineProperty(window, 'fetch', { ...protected })
  Object.defineProperty(window, 'XMLHttpRequest', { ...protected })
  Object.freeze(window.XMLHttpRequest)
  Object.freeze(window.XMLHttpRequest.prototype)
}
```

## The switch(true) Pattern

Helps to avoid repeated of IF-statements.

```js
const user = {
	firstName: 'Some',
	lastName: 'Person',
	email: 'my.address@email.com',
	number: '0044...'
}

switch (true) {
	case !user:
		throw new Error('User must be defined.')
	case !user.firstName:
		throw new Error('First name must be defined')
	case typeof user.firstName !== 'string':
		throw new Error('First name must be a string')
	// ...lots more validation here
	default:
		return user
}
```

Also works in JSX (i.e. [Astro](../webdev/astro.md)) when wrapped in an IIFE:

```jsx
{(() => {
  switch (true) {
    case linkedItem?.system?.type === contentTypes.section.codename:
      return <SectionResolver data={linkedItem as Section} />

    case linkedItem?.system?.type === contentTypes.element.codename:
      return <ElementResolver data={linkedItem as Element} />

    default:
      return <div style="display: none;">
        unmatched component: {linkedItem?.system?.type}
      </div>
  }
})()}
```

## UUIDs

```js
let uniqueId = Date.now().toString(36) + Math.random().toString(36).substring(2);
```

For RFC compatible version 4 GUIDs:

```js
let u = Date.now().toString(16) + Math.random().toString(16) + '0'.repeat(16);
let guid = [u.substr(0,8), u.substr(8,4), '4000-8' + u.substr(13,3), u.substr(16,12)].join('-');
```
