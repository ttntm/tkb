# JavaScript

Notes on JavaScript and an incomplete [Language Reference](./reference/index.html).

## Pages

- [Language Reference](./reference/index.html)
- [Node.js](./nodejs/index.html)
- [Resources](./resources.md)
- [Snippets](./snippets.md)
- [Vue.js](./vue/index.html)

## Async/Await

A good presentation: [async/await (ViennaJS, 28. 11. 2018)](https://ondras.zarovi.cz/slides/2018/viennajs-async-await/)

## Clean Code

See: [Clean Code](../clean-code.md)

## Web & Browsers

### Page Lifecycle API

Some good documentation that describes what's happening in browsers:

[Page Lifecycle API](https://developer.chrome.com/docs/web-platform/page-lifecycle-api)

### Web Events

"The 3 ways (actually 2.5) the Web handles events": [GH Gist](https://gist.github.com/ttntm/f555460c379d769295b8f0e3344dd492)
