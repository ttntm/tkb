# Objects

- introductory read: [Object Oriented Programming in JavaScript](https://www.freecodecamp.org/news/how-javascript-implements-oop/)
- an object is a dynamic collection of properties
  - all object's properties are public
  - the property key is a unique string
    - when a non string is used as the property key, it will be converted to a string
  - the property value can be a primitive, object, or function
- `Object.keys()` can be used to iterate over all properties
- `Object.assign()` copies all properties from one object to another
  - an object can be cloned by copying all its properties to an empty object - `Object.assign({}, srcObject);`

## Constructors

- naming convention: Capitalized
- constructors define properties and behaviors (methods?) instead of returning a value
- use of `this` keyword inside the constructor always refers to the object being created
- `new` operator calls a constructor, telling JS to create a new (object) instance of the constructor
- there's a second way of creating a new object: `Object.create(ConstructorName.prototype)`
  - `Object.create(obj)` creates a new object, and sets obj (the constructor) as the new object's prototype
- constructors are able to accept parameters, in order to make each newly created object have its own property values
- constructors group objects together based on shared characteristics and behavior and define a blueprint that automates their creation - *constructors are therefore something like a class definition for objects*
  - see [How to use JavaScript Classes](https://flaviocopes.com/javascript-classes/)
- the `instanceof` operator returns true/false based on whether or not an object was created using the respective constructor
- theres also a property called "constructor" that contains this information; BUT: it can be overwritten, so better to use `instanceof`
- properties defined by the constructor will become *own properties* of the respective object - in terms of the object having a copy of these properties that can be checked via `hasOwnProperty`
- returns `false` for properties inherited from the prototype
- loop through all these own properties via `for (let property in object) {}`

## Prototype

A recipe for creating objects; means that properties defined there are inherited by all objects created by the respective constructor, thereby reducing the duplication of variables

- see [MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Inheritance_and_the_prototype_chain) for a really detailed explanation
- define via `ObjectName.prototype.PropertyName`
- *never* extend the "system classes", i.e. `Object.prototype`
- all objects (few exceptions - which?) have a prototype
- the protoype is available through the hidden property `__proto__`
- creating multiple properties inside a prototype can be accomplished by passing an object containing these properties: `ObjectName.prototype = {}`
  - when doing this, it's crucial to also set the respective `constructor` property to the correct value - otherwise, JS will default to `Object` as the constructor due to prototypal inheritance and the lack of an association to the actual constructor function when editing its underlying prototype
- `isPrototypeOf` checks if a prototype was inherited from a constructor - `ConstructorName.prototype.isPrototypeOf(NewObject)`

## Prototypal Inheritance

- an object's prototype is actually an object itself
  - that's how/why prototypal inheritance works and also why we need `hasOwnProperty` to check
- distinguish between **supertype** (parent; the constructor) and **subtype** (child; the object created by the constructor)
  - also note `super` - see: [MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/super)
  - shared properties/methods should be created at the supertype level in order to follow DRY and reduce the risk of errors due to duplicated code
  - `Object.create(ConstructorName.prototype)` can also be used to set a subtype's/child's prototype to be an instance of its parent's prototype: `ChildObject.prototype = Object.create(ParentObject.prototype)`
  - careful: when an object inherits its prototype from another object, it also inherits the supertype's constructor property.
  - when inheriting from a supertype, it can be necessary to use `ConstructorName.prototype.constructor = ConstructorName` in order to set the constructor to the desired subtype
  - override inherited methods: define them again on the child level using the same name as the parent's method
- visually appealing explanation: <https://dev.to/lydiahallie/javascript-visualized-prototypal-inheritance-47co>

## Closure

- a function always has access to the context in which it was created
- use this to define a private variable inside a constructor and a method to access it - `object.property` won't give access, but the *privileged method* `object.showProperty()` will

## Dates

The Date constructor's `month` argument **ranges from 0-11 instead of 1-12**.

According to B. Eich, "JS's Date is a copy of Java's JDK1.0 (1995) `java.util.Date`" and there was "no time" to invent a better API or to fix Java's.

## Mixins

- a mixin allows unrelated objects (no similar parent, thus no inheritance; i.e. birds and planes) to use a collection of functions/methods
- define as `let mixinName = function(obj) { obj.newMethod = function() { method } };`

## Immediately Invoked Function Expression (IIFE)

- excecutes as soon as declared
- `(function(){dosomething})();`
- the two parentheses at the end of the function expression cause it to be immediately executed/invoked
- often used to group related functionality into a single object or **module**
  - when doing that, and IIFE returns an object that contains the mixins: `let moduleName = (function() { return { mixinName: function(obj) { obj.newMethod = function() { method }; } } })();`
  - advantage: all of the related behaviors can be packaged into a single object that can then be used by other parts of the code
  - this is known as the *module pattern*
