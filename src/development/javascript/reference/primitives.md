# Primitives

- Number, boolean, string, undefined and null are primitives
- Primitives (except null and undefined) are treated like objects, in the sense that they have methods, but they are not objects.
- Numbers, strings, and booleans have object-equivalent wrappers. These are the `Number`, `String`, and `Boolean` functions.
  - In order to allow access to properties on primitives, JavaScript creates a wrapper object and then destroys it. The process of creating and destroying wrapper objects is optimized by the JavaScript engine (see [JavaScript Visualized: the JavaScript Engine](https://dev.to/lydiahallie/javascript-visualized-the-javascript-engine-4cdf)).
    - Primitives are immutable, and objects are mutable
- The language has *truthy* and *falsy* values
  - `false`, `null`, `undefined`, `''`(empty string), `0` and `NaN` are falsy. All other values, including all objects, are truthy
  - The truthy value is evaluated to true when executed in a boolean context, falsy value is evaluated to false
  - check this with `if (!value)`
    - see: [JS Essentials: Falsy Values](https://www.samanthaming.com/tidbits/25-js-essentials-falsy-values)

## Number

- There is only one number type in JavaScript, the 64-bit binary floating point type. Decimal numbers' arithmetic is inexact
- Numbers inherit methods from the `Number.prototype` object
- converting strings to numbers:
  - `Number.parseInt()`
  - `Number.parseFloat()`
  - `Number()`
- string -> float incl. keeping trailing zeros:
  - `parseFloat(string).toFixed(*)` where * is the number of trailing digits
  - see: [Stack Overflow](https://stackoverflow.com/questions/24889498/javascript-keep-trailing-zeroes)
  - *DON'T do this, if it's necessary to do math with those numbers*
- `Number.isNaN()` can detect `NaN`
  - result of invalid arithmetric operations

## String

- A string stores a series of Unicode characters. The text can be inside double quotes "" or single quotes ''.
- string is immutable
- Strings inherit methods from `String.prototype`
  - `substring()`
  - `indexOf()`
  - `concat()`
  - `split()`
    - takes RegEx as an argument, very handy
    - `str.split(/\W/g)` will split a string at every special character like space or punctuation
  - `trim()`
    - to remove whitespace from both ends of a string
