# Arrays

Arrays are indexed collections of values. Each value is an element. Elements are ordered and accessed by their index number.

JavaScript has array-like objects. Arrays are implemented using objects. Indexes are converted to strings and used as names for retrieving values.

In general, using the `slice()` method instead of `splice()` helps to avoid any array-mutating side effects.

Use `array.concat(otherArray)` to concatenate arrays or to add items to the end of an array. Use instead of `push` in order to a void mutations of the original input data - complies with the concept of functional programming.

`Array.prototype.map()` or `map()`:

  - built-in method for Arrays
  - iterates over each item in an array and returns a new array containing the results of the callback function passed to `map()`
    - callback is passed 3 arguments:
      - the current element
      - its index
      - the array that `map()` was called upon
  - does *not* mutate the original array
  - returns an array of the same length as the one is was called upon
    - is a pure function thus, output solely depends on its input
  - see [MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map) for examples re: callback syntax

`Array.prototype.filter()` or `filter()`

  - built-in method for Arrays
  - iterates over each item in an array and returns a new array consisting only of those items that the callback function evaluated to `true` for
    - in short: filters the array based on a function
  - callback is passed 3 arguments:
    - the current element
    - its index
    - the array that `filter()` was called upon
  - does *not* mutate the original array

`Array.protype.reduce()` or `reduce()`

  - built-in method for Arrays
  - allows for general array processing; iterates over an array and returns a single value based on a callback function
  - callback is passed 4 arguments
    - accumulator - the return value of the previous iteration of the callback function
    - the current element
    - its index
    - the array that `reduce()` was called upon
  - there's an additional parameter in addition to the callback function that takes the *initial value for the accumulator*
    - if this value isn't used, the first iteration skip the accumulator and the second gets assigned the array's first value as its accumulator
    - this value can also be an object that `reduce` then adds properties and values to

`Array.prototype.sort()` or `sort()`

  - this method sorts the elements of an array according to the callback function
  - JS default sorting method is by string Unicode point value, which may return unexpected results
    - better to provide a callback function (*compare function*) to specify how to sort the array items
  - compare function logic
    - If `compareFunction(a,b)` returns a value **less than 0** for two elements a and b, then *a will come before b*
    - If `compareFunction(a,b)` returns a value **greater than 0** for two elements a and b, then *b will come before a*
    - If `compareFunction(a,b)` returns a value **equal to 0** for two elements a and b, then *a and b will remain unchanged*

`Array.prototype.join` or `join()`

Will join an array into a string with the given separator, i.e. `arr.join(' ')`.

`Array.prototype.every` or `every()`

  - checks if *every* element inside an array meets a criteria
  - returns `true` or `false`

`Array.prototype.some` or `some()`

  - checks if *any* element inside an array meets a criteria
  - returns `true` or `false`

## Performance Considerations

Avoid chaining array methods that produce new arrays that get discarded immediately, i.e. `data.map(...).filter(Boolean)`.

Use `reduce()` or loops instead.
