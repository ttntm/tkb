# JS FAQ

## Arrays

`sort()`, but without mutating the original array: [Array.prototype.toSorted()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/toSorted)

`Array.filter(Boolean)`: remove `null` and `undefined` values from an array

### Deduplication

...using [Set.prototype.union()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set/union):

```js
const a = ['a','b','c']
const b = ['b', 'b', 'b', 'd', 'z']

console.log(new Set(a).union(new Set(b)))

// Set (5) { "a" => "a", "b" => "b", "c" => "c", "d" => "d", "z" => "z" }
```

## DOM Events

`DOMContentLoaded` attached to `document`, never to `window`.

[Document: DOMContentLoaded event](https://developer.mozilla.org/en-US/docs/Web/API/Document/DOMContentLoaded_event)

## Loops

The difference between `for..in` and `for..of`:

- `for..in` iterates over all enumerable *property keys of an object*
- `for..of` iterates over the *values of an iterable object* (i.e. arrays, maps, sets, NodeLists)

## Operators

### Nullish coalescing

The nullish coalescing (`??`) operator is a logical operator that returns its right-hand side operand when its left-hand side operand is `null` or `undefined`, and otherwise returns its left-hand side operand.

[Nullish coalescing operator (??) - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing)
