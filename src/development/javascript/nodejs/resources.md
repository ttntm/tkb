# Node.js Resources

- [bulletproof-nodejs](https://github.com/santiq/bulletproof-nodejs)
  - [Bulletproof node.js project architecture](https://softwareontheroad.com/ideal-nodejs-project-structure/)
- [Compatability Table](https://node.green)
- [Node.js + MySQL - Boilerplate API with Email Sign Up, Verification, Authentication & Forgot Password](https://jasonwatmore.com/post/2020/09/08/nodejs-mysql-boilerplate-api-with-email-sign-up-verification-authentication-forgot-password)
- Recurring tasks:
  - [Cron jobs and task scheduling](https://www.softwareontheroad.com/nodejs-scalability-issues/#jobs)
  - [agenda](https://github.com/agenda/agenda) (Lightweight job scheduling for Node.js)
- [worker queue for nodejs?](https://stackoverflow.com/questions/16904093/worker-queue-for-nodejs)

## Architecture

- [How to Architect a Node.Js Project from Ground Up?](https://dev.to/shadid12/how-to-architect-a-node-js-project-from-ground-up-1n22)

## Data Storage

- [mongoose](https://mongoosejs.com) (MongoDB object modeling)

## Learning

- [learnyounode](https://github.com/workshopper/learnyounode)
- [NodeJS Course](https://www.theodinproject.com/paths/full-stack-javascript/courses/nodejs)

### Azure

- [Deploy a Node.js + MongoDB web app to Azure](https://learn.microsoft.com/en-us/azure/app-service/tutorial-nodejs-mongodb-app)
  - [msdocs-nodejs-mongodb-azure-sample-app](https://github.com/Azure-Samples/msdocs-nodejs-mongodb-azure-sample-app)
