# Node.js

Node.js topic index page.

## Pages

- [Express](./express.md)
- [Resources](./resources.md)

## Environment Variables

No need for `dotenv` when using version `20.6.0` or higher.

We can do this now:

`node --env-file=.env your-script.js`

And then use it in the script like this:

```js
console.log(process.title);  // Outputs: Sample Node App
console.log(`Hello, ${process.env.USER_NAME}`);  // Outputs: Hello, John Doe
```

Also, `process.loadEnvFile()` is available from version `21.7.0`:

```js
process.loadEnvFile();  // Automatically loads `.env` from the current directory
​
// Or specify a path
process.loadEnvFile('./config/env_vars.env');
```

And we can also use `parseEnv()` to obtain an object containing the environment variables:

```js
const util = require('node:util');
const envVars = util.parseEnv('API_KEY=12345');
console.log(envVars.API_KEY);  // Outputs: 12345
```

## NPM

The main differences between using `npm install` and `npm ci` are:

- The project must have an existing `package-lock.json` or `npm-shrinkwrap.json`.
- If dependencies in the package lock do not match those in `package.json`, `npm ci` will exit with an error, instead of updating the package lock.
- `npm ci` can only install entire projects at a time: individual dependencies cannot be added with this command.
- If a `node_modules` folder is already present, it will be automatically removed before `npm ci` begins its install.
- It will never write to `package.json` or any of the package-locks: installs are essentially frozen.

**Good practice**: prefer `npm ci` in local development and especially in deployment pipelines.

## NVM

Node Version Manager

CLI: see [Commands](../../../software/cli.md)

Tutorial: [How To Run Multiple Versions of Node.js with Node Version Manager](https://www.digitalocean.com/community/tutorials/nodejs-node-version-manager)

## Watch mode

Native from v20+

`node --watch app.js`

or

`node --watch 'lib/**/*.js' app.js`
