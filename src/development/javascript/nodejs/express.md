# Express

> Express is a popular web framework, written in JavaScript and hosted within the Node.js runtime environment.

Docs: [expressjs.com](https://expressjs.com)

## Middleware

File uploads: use [Multer](https://expressjs.com/en/resources/middleware/multer.html)

## Notes

Parsing HTML form data: use [express.urlencoded([options])](https://expressjs.com/en/api.html#express.urlencoded).

Parsing JSON: use [express.json([options])](https://expressjs.com/en/api.html#express.json)

## Resources

- [Express web framework](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs)
- [x] [Node.js and Express](https://fullstackopen.com/en/part3/node_js_and_express)
- [node-express-mysql-boilerplate](https://github.com/mangya/node-express-mysql-boilerplate)
- [How to build a real time chat application in Node.js using Express, Mongoose and Socket.io](https://www.freecodecamp.org/news/simple-chat-application-in-node-js-using-express-mongoose-and-socket-io-ee62d94f5804/)
