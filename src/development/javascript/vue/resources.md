# Vue.js Resources

<div class="warning">

**The content below was written a long time ago (vue2) and is most likely OUTDATED**.

It should be reviewed and updated.
<br><br>
</div>

## Get started

- [https://www.taniarascia.com/getting-started-with-vue](https://www.taniarascia.com/getting-started-with-vue/)
- [https://css-tricks.com/methods-computed-and-watchers-in-vue-js](https://css-tricks.com/methods-computed-and-watchers-in-vue-js/)
- [Intro to vue.js](https://css-tricks.com/intro-to-vue-1-rendering-directives-events/)
- [https://notes-on-vue.netlify.app](https://notes-on-vue.netlify.app/)

## Extensions

- vue router: [https://router.vuejs.org](https://router.vuejs.org/)
- vuex: [https://vuex.vuejs.org](https://vuex.vuejs.org/)
- Bootstrap Vue: [https://bootstrap-vue.org](https://bootstrap-vue.org/)
- Tailwind: [https://markus.oberlehner.net/blog/setting-up-tailwind-css-with-vue](https://markus.oberlehner.net/blog/setting-up-tailwind-css-with-vue/)
  - but: tailwind includes purgecss by default now, so no need to install that separately
- PWA: <https://cli.vuejs.org/core-plugins/pwa.html>

## CMS options

See: [CMS](../../webdev/cms/index.html)

## Testing

[Unit testing Vuex modules with Jest](https://blog.logrocket.com/unit-testing-vuex-modules-jest/)

## Boilerplates/Templates/How-Tos

### Multi Page App

- <https://itnext.io/anyway-heres-how-to-create-a-multiple-layout-system-with-vue-and-vue-router-b379baa91a05>
  - <https://github.com/darkylmnx/Layout-system-with-vue-and-vue-router>

### List app

- <https://codepen.io/cahdeemer/pen/OryxWG>

### vue + netlify functions

provides an extremely helpful local function proxy

- [https://alligator.io/nodejs/solve-cors-once-and-for-all-netlify-dev](https://alligator.io/nodejs/solve-cors-once-and-for-all-netlify-dev/)
  - ***this was essential!***

### vue + netlify identity

simple to used *if using the pre-built widget* - otherwise refer to the resources related to fauna db → multi user app with indiv. user content

- basics: <https://github.com/whizjs/netlify-identity-demo-vue>
- better (no local storage used): <https://github.com/jeremywynn/nuxt-netlify-identity>
  - explained here: <https://dev.to/jeremywynn/using-netlify-identity-in-a-vue-spa-without-localstorage-23ob>
- <https://www.netlify.com/blog/2018/12/07/gotrue-js-bringing-authentication-to-static-sites-with-just-3kb-of-js/>
- <https://github.com/shortdiv/gotruejs-in-vue>

### fauna DB

- [Building Serverless CRUD apps with Netlify Functions & FaunaDB | Netlify](https://www.netlify.com/blog/2018/07/09/building-serverless-crud-apps-with-netlify-functions-faunadb/)
  - <https://github.com/netlify/netlify-faunadb-example>
- <https://dev.to/chiubaca/build-a-serverless-crud-app-using-vue-js-netlify-and-faunadb-5dno>
  - use case: multi-user app with separate content *per user*, i.e. users write to db with their own token, seeing only their own content etc.
- <https://dev.to/machy8/jamstack-e-commerce-using-apicart-faunadb-gridsome-and-netlify-39jm>

### Storing images

- <https://medium.com/js-dojo/how-to-upload-base64-images-in-vue-nodejs-4e89635daebc>
- <https://support.cloudinary.com/hc/en-us/articles/360011139699-Creating-a-Vue-js-Application-for-Uploading-Assets-to-Cloudinary>
  - <https://github.com/cloudinary-devs/training-vuejs>
  - note: the API needs the data passed through `FormData()` in order to be accepted - otherwise there's going to be a 400 Bad Request error

### Loading images

- [https://alligator.io/vuejs/vue-lazy-load-images](https://alligator.io/vuejs/vue-lazy-load-images/)
  - <https://github.com/matheusgrieger/vue-clazy-load>
- <https://github.com/cube-inc/cube-vue-image-lazy>

### Lightbox

- <https://github.com/silencesys/silentbox>

### Renderless components

- [https://adamwathan.me/renderless-components-in-vuejs](https://adamwathan.me/renderless-components-in-vuejs/)
- <https://vuejs.org/v2/guide/render-function.html>

### vuex and scaling apps

- [Making a large scale app with vue.js (part 1): modularize your store](https://medium.com/@stephane.souron/making-a-large-scale-app-with-vue-js-part-1-modularize-your-store-bf9066436502)
- [Making a large scale app with vue.js (part 2): a little bit of Object Oriented Programming](https://medium.com/@stephane.souron/making-a-large-scale-app-with-vue-js-part-2-a-little-bit-of-object-oriented-programming-6822de5ef19d)
  - very good article, key points:
    - use store as a "single source of truth"
    - don't deal with data in components/views, use store actions/mutations
    - use custom Object Class to deal with your data
- [https://alligator.io/vuejs/vuex-persist-state](https://alligator.io/vuejs/vuex-persist-state/)

### vuex > pinia

- <https://blog.canopas.com/vue-state-migrate-from-vuex-store-to-pinia-7db0eab864fd>

### Notifications

- <https://laravel-news.com/building-a-flash-message-component-with-vue-js-and-tailwind-css>
