# Vue.js

Vue.js topic index page.

<div class="warning">

**The content below was written a long time ago (vue2) and is most likely OUTDATED**.

It should be reviewed and updated.
<br><br>
</div>

## Pages

- [Composables](./composables.md)
- [Resources](./resources.md)

## Templates

`.vue` files in `./src/components` -> they're called *Single File Components*

Consists of 3 main sections: - `<template>` - `<script>` - `<style scoped>`

`scoped` → only applies to this component and not globally

In Vue, the convention is that the filename and import will be in *PascalCase* BUT: Vue internally automatically creates an alias from user-name to UserName , and vice versa, so you can use whatever you like. It's generally best to use UserName in the JavaScript, and user-name in the template.

Templates can only have one root child element -> wrap content in one `<div>` to make things easier when more stuff is added

In `App.vue` -> for the `import`, we can use `@` to reference the `src` folder. `App.vue` knows which components it can use via the `components` property. All imported components must be added there.

Target specific template components? We can do that with [refs](https://vuejs.org/v2/api/#vm-refs) -> `<input ref="first" ... />` and `this.$refs.first.focus()`

`slot` → think of hugo's shortcodes: defined template code that can still consume other template (HTML) code added between its opening and closing brackets

### Slots

- <https://michaelnthiessen.com/supercharge-your-slots>

## Data

### Reactivity

Passing data (method `data()`) to component in `App.vue` → we can do that by passing the data down as a property via `props: {}`.

An attribute that begins with a colon `:` will allow you to pass data. The more verbose version would be `v-bind`.

The respective component needs to receive the data - that works via `props: {}` inside the `export default` within that component

Once the component has the data available, we'll have to loop through it somehow in order to display it -> that's what `v-for` is for (think of hugo `{{ range }}`)

*Vue (like React) has a requirement for uniquely identifying any element in an array, so we'll use `:key` to read the respective item's ID*

Styles: `v-bind:style` needs to be passed an object for it to work properly with `item in items` (range) data input

### v-model

[v-model](https://vuejs.org/v2/guide/forms.html) is some built-in Vue syntactic sugar for updating an input value with an onchange event.

- use with normal JS object dot notation
- updates the respective component state with the input value

### Event listeners

**Forms**: We want to do an `onsubmit` event on the form. We can do that with `v-on:submit`, or `@submit` for short.

This convention will be the same for `@click`/`v-on:click`or any other similar event.

`@submit` can also be combined with `prevent` to avoid default (form) behaviour: `@submit.prevent="MethodName"`

### Computed properties

In Vue, we can use [computed properties](https://vuejs.org/v2/guide/computed.html), which are functions that are automatically computed when something changes.

This way we can avoid putting complex logic in the Vue template itself.

### Methods

Created per component inside the export

### Emitting data from component to app

That's what `$emit` is for: `this.$emit('name-of-emitted-event', dataToPass)`

**Syntax**

The `add:employee` syntax (as opposed to `add-employee` or something else) is recommended in [the Vue documentation](https://vuejs.org/v2/guide/components-custom-events.html#sync-Modifier)

### Retrieving events from child components

In `App.vue` → component needs to acknowledge and handle the emitted event like this: `<component @name-of-emitted-event="methodToCallOnceEmitted"></component>`

The method to handle the emitted event needs to be defined in `App.vue` as well

## State

Keep in mind that state can be used to create `v-if` - `v-else` constructs when editing existing objects. This is then handled by the UI in `<template>` and can be used to make table content editable for example

### vuex

> Vuex provides us with a *store* for our state. This store will have variables with *getters*, and we will only be able to change these with *mutations*. To simplify commiting those mutations, we'll use *actions*.

From: <https://paweljw.github.io/2017/10/vue.js-front-end-app-part-4-keeping-state-with-vuex/>

There are **four things that go into a Vuex module**:

- the initial state
- getters
- mutations
- actions

In depth summary: <https://dev.to/decoeur_/managing-state-with-vuex---the-guide-i-wish-id-had-28h>

## vue-router

Be very careful with manual `<a href="#">Foo</a>` kind of links - they will reset the app's state and *cause effects similar to page reload,* i.e. mess with static data.

### Navigation Guards

...are primarily used to guard navigations either by redirecting it or canceling it. There are a number of ways to hook into the route navigation process: globally, per-route, or in-component.

See: <https://router.vuejs.org/guide/advanced/navigation-guards.html>

## Code splitting

Webpack/general → <https://stackoverflow.com/questions/51816020/how-to-break-the-js-files-into-chunks-in-vue-cli-3-with-webpack-performance-obje>

### Components

- <https://vuejsdevelopers.com/2017/07/03/vue-js-code-splitting-webpack/>
- <https://vuejs.org/v2/guide/components-dynamic-async.html>

## Router

- <https://medium.com/kaliop/how-to-use-webpack-code-splitting-with-vue-js-112ab7a821ee>
- <https://stackoverflow.com/questions/55042331/vue-cli-code-splitting-creates-a-file-with-very-long-name-that-cant-be-served>

## Nuxt

- <https://masteringnuxt.com/blog/24-time-saving-tips-for-nuxt3>
