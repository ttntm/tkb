# Composables

Applies to: vue3

- state reset: <https://medium.com/@jafn28/a-novel-way-of-resetting-state-with-vue-composition-api-a6713548edf2>
- Suspense component: <https://vuejs.org/guide/built-ins/suspense.html>

## Destructuring

Destructuring a value from a reactive object will break reactivity:

```javascript
const myObj = reactive({ prop1: 'hello', prop2: 'world' })
const { prop1 } = myObj
```

=> `prop1` is just a plain String here. The reactivity comes from the object itself and not the property you're grabbing.

You must use toRefs to convert all of the properties of the object into refs first, and then you can destructure without issues. This is because the reactivity is inherent to the ref that you're grabbing:

```javascript
const myObj = reactive({ prop1: 'hello', prop2: 'world' })
const { prop1 } = toRefs(myObj)
```

=> Now `prop1` is a `ref`, maintaining reactivity.

Using `toRefs` in this way lets us destructure our props when using script setup without losing reactivity:

```javascript
const { prop1, prop2 } = toRefs(
  defineProps({
    prop1: {
      type: String,
      required: true
    },
    prop2: {
      type: String,
      default: 'World'
    }
  })
)
```
