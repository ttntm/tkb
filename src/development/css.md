# CSS

## a11y - Testing

```css
, :hover {
  cursor: none !important;
}
```

> That forces you to use the keyboard. If you find something that makes it hard or impossible to do using the keyboard, fix it! Not just for yourself but for everyone relying on keyboard accessibility.

Source: [@matuzo/110671054774143915](https://elk.zone/front-end.social/@matuzo/110671054774143915)

## Aligning Things

`align` vs `justify`: think of an inline flow, where the `justify` keyword of `text-align` controls the text in an `inline` direction. And for `align` think of `vertical-align`, which controls it over the block axis.

`items` vs `content`: an "item" is something that can be controlled by itself, via `*-self` properties. `content` is a single entity, where you align the whole chunk of things, without a way to override it per item.

Source: [@kizu/112432872791295982](https://elk.zone/front-end.social/@kizu/112432872791295982)

--

`align:content: center;`: works on regular `display: block;` `div` elements.

It's great when working with a [CMS](../development/webdev/cms/index.html).

## Chaining State Selectors

```css
.foo:is(:hover, :focus, :active) {
  background: purple;
  border: 1px solid blue;
}
```

## Detect JavaScript Support

```css
@media (scripting: enabled) {
  .my-element {
    /* enhanced styles if JS is available */
  }
}
```

```css
@media (scripting: none) {
  .my-element {
    /* fallback styles when JS is not supported */
  }
}
```

Source: [csscade.com](https://www.csscade.com/detect-javascript-support)

## Handle Reduced Motion

```css
@media (prefers-reduced-motion) {
  *,
  *::before,
  *::after {
    scroll-behavior: auto;
  }

  *:active {
    transform: none;
  }

  *:hover {
    transition: none;
    transition-duration: 0ms;
  }
}
```

## Image Inversion

```css
.auto-invert {
  filter: grayscale(50%) invert(100%) brightness(95%) hue-rotate(180deg);
}
```

## Nesting Media Queries

We can do this now:

```css
.my-element {
  @media (min-width: 500px) {
  }
  @media (min-width: 700px) {
  }
  @media (min-width: 800px) {
  }
}
```

...and combine it with container queries:

```css
.parent {
  container-name: parent;
  container-type: inline-size;
}

.child {
  @container parent (width >= 1000px) {
    background: yellow;
  }
}
```

## Variables: Advanced Use Cases

To remember: scoping variables to elements, i.e. buttons

See: [An advanced way to use CSS variables](https://gomakethings.com/an-advanced-way-to-use-css-variables/)

## Wrapping Text

`text-wrap: balance;` is a great option, and also works for images (i.e. icons).
