# Miscellaneous

KB entries that didn't fit into one of the bigger topics.

## Infographics

- [Map of Physics](./map-of-physics.md)
- [The 5 Levels of Hype](./5-levels-of-hype.md)
- [The Problem With Plastics](./plastics.md)
