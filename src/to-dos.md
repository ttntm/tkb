# To-Dos

Ideas and to-dos for this knowledge base:

- [x] Use tables instead of lists of commands in [software/commands](./software/cli.md)
- [x] Create an index of all the dedicated "resources" pages in this kb
- Consider automatic deployments of this website
  - [Deploying mdbook to codeberg pages using woodpecker CI](https://www.markpitblado.me/blog/deploying-mdbook-to-codeberg-pages-using-woodpecker-ci/)
