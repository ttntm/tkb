# Privacy

## Browsers

- Switch to [FF](./firefox.md) on all devices
  - Add uBlock (+ [hagezi/dns-blocklists](https://github.com/hagezi/dns-blocklists))
  - use DuckDuckGo instead of google
  - Never stay signed in to google (i.e. gmail)
- Uninstall Chrome on all devices
- Revise privacy settings, esp. 3rd party cookies
- Disable auto sign in for ~anything~ most things

### Firefox

See: [Firefox](./firefox.md)

## Google Account Settings

- Revise privacy settings
- Turn off location history
- Turn off ad personalization

## VPNs

Be careful: [TunnelVision vulnerability](https://arstechnica.com/security/2024/05/novel-attack-against-virtually-all-vpn-apps-neuters-their-entire-purpose/)

## Windows

- Get a base config from [privacy.sexy](https://privacy.sexy).
- [Disable activity history](https://support.microsoft.com/en-us/help/4468227/windows-10-activity-history-and-your-privacy-microsoft-privacy)
- Uninstall social apps
- Check and deactivate all "report usage data" settings

## Resources

- [12ft.io](https://12ft.io) (view page without JS)
- [AdGuard DNS](./adguard-dns.md)
- [Awesome Privacy](https://pluja.github.io/awesome-privacy/)
- [Cryptomator](https://cryptomator.org)
- [Exodus Privacy](https://exodus-privacy.eu.org) (check Android apps)
- [Identity Leak Checker](https://sec.hpi.de/ilc/)
- [Prism Break](https://prism-break.org/en/)
- [Privacy Guides](https://www.privacyguides.org/en/)
- [Privacy Tools](https://www.privacytools.io)
- [restoreprivacy.com](https://restoreprivacy.com/)
  - [Browser Fingerprinting Protection](https://restoreprivacy.com/browser-fingerprinting/)
- [Shufflecake](https://shufflecake.net/)
- [switching.software](https://switching.software/)
- [The Hitchhiker's Guide to Online Anonymity](https://anonymousplanet.org/guide.html)
