# Android

## ADB

Wireless debugging (developer options) > pair with IP address/port and code.

Then `adb connect 1.2.3.7:12345` and `adb shell`.

List packages: `pm list packages`, or something like `pm list packages | grep 'google.android*'`

Disable package: `pm disable com.google.android.PKG_NAME`

Uninstall package: `pm uninstall --user 0 com.google.android.PKG_NAME`

NB: use the optional flag `--user 0` to remove system apps.

## Links & Resources

- [Werbung/Tracker schnell und einfach systemweit loswerden](https://www.kuketz-blog.de/android-werbung-tracker-schnell-und-einfach-systemweit-loswerden/)
