# Firefox

## Known Issues

### Web Fonts not loading

FF ignores the CSS rules that specify fonts that should be available on almost all systems.

From the FF v119.0 release notes:

> The visibility of fonts to websites has been restricted to system fonts and language pack fonts in Enhanced Tracking Protection strict mode to mitigate font fingerprinting.
>
> To get around that:
> - go to FF Menu > Settings > Privacy & Security
>   - change Strict to Custom
>   - change Suspected fingerprinters to 'Only in private windows'
>   - change Tracking content to 'All windows'

## Settings

Last update: 04/2024

| Flag | Value | Comment |
|---------|---------|---------|
| `browser.translations.automaticallyPopup` | `false` | Disable automatic translations |
| `dom.event.clipboardevents.enabled` | `false` | Will break some stuff, i.e. c/p links in Notion |
| `dom.push.enabled` | `false` | Disables Push-API hosted at Google |
| `geo.enabled` | `false` | Disables geolocation tracking |
| `media.eme.enabled` | `false` | Disables playback of DRM-controlled HTML5 content |
| `media.navigator.enabled` | `false` | blocks tracking microphone and camera status |
| `media.peerconnection.enabled` | `false` |  |
| `network.dns.disablePrefetch` | `true` |  |
| `network.prefetch-next` | `false` |  |
| `privacy.firstparty.isolate` | `true` |  |
| `privacy.resistFingerprinting` | `true` | *Will mess with the window size* |
| `privacy.trackingprotection.enabled` | `true` |  |
| `privacy.trackingprotection.cryptomining.enabled` | `true` |  |
| `privacy.trackingprotection.fingerprinting.enabled` | `true` |  |
| `toolkit.telemetry.enabled` | `false` | Disables FF telemetry |
| `webgl.disabled` | `true` |  |

Some of these were taken from [Privacy Guide](https://restoreprivacy.com/firefox-privacy/).
