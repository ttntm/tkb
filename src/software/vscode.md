# VS Code

## Customizations

- [Snippet Generator](https://snippet-generator.app)
- [Themes](https://vscodethemes.com)

### Disable Shadows

[Is it possible to remove shadow from VS Code?](https://stackoverflow.com/a/48229279)

=> YES!

```json
"workbench.colorCustomizations": {
	"scrollbar.shadow": "#0000",
	"widget.shadow": "#0000"
},
```

## Force Type-Checking for JavaScript

Enable `implicitProjectConfig.checkJs` in the settings.

### Horizontal Scrollbar

```json
"editor.scrollbar.horizontal": "hidden"
```

### IntelliSense

```json
"editor.acceptSuggestionOnEnter": "off",
"editor.acceptSuggestionOnCommitCharacter": false
"editor.quickSuggestions": {
  "other": true,
  "comments": false,
  "strings": false
},
"editor.suggest.showWords": false,
"editor.wordBasedSuggestions": "currentDocument",
"emmet.showSuggestionsAsSnippets": true,
```

## Extensions

Obtained from the CLI using `code --list-extensions`.

- astro-build.astro-vscode
- bradlc.vscode-tailwindcss
- ginfuru.better-nunjucks
- maattdd.gitless
- mechatroner.rainbow-csv
- meganrogge.template-string-converter
- mjmlio.vscode-mjml
- ms-vscode.live-server
- neo-ltex.ltex
- sergey-agadzhanov.ampscript
- tamasfe.even-better-toml
- vscode-icons-team.vscode-icons
- Vue.volar
- vunguyentuan.vscode-postcss
- yatki.vscode-surround
- Zignd.html-css-class-completion

Last update: 02/2024

### GitLens

Turn off `gitlens.integrations.enabled` to remove the notification dot in the Accounts menu.

### Recommending Extensions

Add a `.vscode` folder, if not already there for your debugging setup, and create a new file named `extensions.json` with the following content:

```json
{
	"recommendations": [
		"astro-build.astro-vscode"
	]
}
```

`recommendations[]` contains the extensions identifier from the Visual Studio Marketplace.

Source: [Recommending VSCode extensions within your Open Source projects](https://tattoocoder.com/recommending-vscode-extensions-within-your-open-source-projects/)
