# Commands

Frequently used CLI tools / commands.

## Resources

- [Comprehensive Linux cheat sheet](https://gto76.github.io/linux-cheatsheet/)
- [20 Basic 'ls' Command Examples in Linux](https://www.tecmint.com/15-basic-ls-command-examples-in-linux/)

## Basic

| Command | Function |
|---------|---------|
| `cat` | print file content into terminal |
| `ls` | list dir content, use as `ls -hl` for detailed output |
| `acpi -V` | acpi (and battery) stats |
| `cal` | displays a calendar of the current month |
| `id3v2` | CLI for MP3 metadata; run without args to see the options |
| `inxi -v7` | detailed system info |
| `lolcat` | use with piped input for rainbow text output |
| `lsblk` | list block devices, i.e. all hard drives with partitions as `lsblk -f` |
| `lsusb` | list usb devices |
| `powertop` | detailed power consumption stats |
| `ps` | list running (terminal) processes |
| `neofetch` | system info |
| `top` / `btop` | task mgr |
| `upower -i /org/freedesktop/UPower/devices/battery_BAT0` | battery stats |
| `watch -n1 sensors` | temps of core hardware (`-n1` being the update interval, 1 second here) |

## Tools

### ATAC

ATAC is *A*rguably a *T*erminal *A*PI *C*lient.

Docs: [GH Wiki](https://github.com/Julien-cpsn/ATAC/wiki)

### cmus

**Refresh media library**

`:clear`

`:add /path/to/music`

[Feature request: Auto-update library · Issue #233 · cmus/cmus](https://github.com/cmus/cmus/issues/233#issuecomment-399426415)

#### Useful Keys

| Key | Function |
|---------|---------|
| `v` | stop playback |
| `b` | next track |
| `z` | previous track |
| `c` | pause playback |
| `s` | toggle shuffle (read about the m key below if you're going to use shuffle) |
| `m` | toggles the "aaa mode"; aaa stands for artist, album, or all |
| `x` | restart track |
| `i` | focus selection bar on the currently playing track (handy when in shuffle mode and follow is off) |
| `f` | toggle follow. When follow is on, the selection bar will jump the track that is currently playing |
| `/` | Search. Type /, a string, and enter to find the first instance of that string in your current view |
| `-` | reduce volume by 10% |
| `+` | increase volume by 10% |

### cURL

```bash
curl -X POST -H "Content-Type: application/json" -d '{"key": "value"}' https://example/contact
```

### dog

DNS lookup

had to build from source on fedora - see <https://www.linode.com/docs/guides/use-dog-linux-dns-client/>

usage example:

`dog github.com A AAAA MX NS TXT`

### exa

A modern replacement for `ls`.

Usage: `elhg`

(An alias for `exa --long --header --git`)

Docs: [the.exa.website/introduction](https://the.exa.website/introduction)

### ffmpeg

Download a blob video based on the playlist file:

`ffmpeg -i 'https://xmpl.com/video.m3u8' -bsf:a aac_adtstoasc -vcodec copy -c copy -crf 50 file.mp4`

Some more information about downloading videos can also be found here: [Downloading videos (the hard way)](https://blog.thym.at/p/downloading-videos/)

### ghostscript

Compress PDFs

`gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH -sOutputFile=compressed_PDF_file.pdf input_PDF_file.pdf`

### git

See: [GIT](../development/git.md)

### ImageMagick

[Command-line Basics: Resizing Images with ImageMagick  | DigitalOcean](https://www.digitalocean.com/community/tutorials/workflow-resizing-images-with-imagemagick)

Use to convert images:

`magick aaa_vector.svg aaa_pixel.jpg`

Resize images:

`magick -resize 600^x300 image.jpg resized.jpg`

The `^` makes sure that the marked size gets followed while the other one gets ignored (if applicable).

Square cropping:

`magick -resize 300x300^ -gravity center -extent 300x300  image.jpg cropped.jpg`

SVG to PNG (preserving transparency):

`magick -background none in.svg out.png`

### lazygit

Key bindings: [lazygit/Keybindings_en.md at jesseduffield/lazygit](https://github.com/jesseduffield/lazygit/blob/master/docs/keybindings/Keybindings_en.md)

`gaa` + `gcommit` + `git push` ⇒ `a`, `c`, `P`

### nvm

| Command | Function |
|---------|---------|
| `nvm ls` | list locally installed versions of node |
| `nvm ls-remote` | list remote available versions of node |
| `nvm install 18.16.1` | install specific version of node |
| `nvm use 20.5.1` | switch version of node |
| `nvm install --lts` | install latest LTS version of node |
| `nvm install stable` | install latest stable version of node |

### openconnect (VPN)

Use a custom config file to import the connection into network mgr:

`nmcli connection import type openvpn file [file].ovpn`

### pacman

`sudo pacman -Rns pkgname` → remove package called `pkgname`

### SSH

Force SSH to use a specific identity:

```bash
ssh -F /dev/null -o IdentitiesOnly=yes -i <private key filename> <hostname>
```

Source: [superuser.com/a/1777452](https://superuser.com/a/1777452)

### superfile

`spf`

Docs: [superfile](https://superfile.netlify.app/overview/)

Keyboard shortcuts: [Hotkey list](https://superfile.netlify.app/list/hotkey-list/)

### zip

files into archive: `zip archive.zip file1 file2`

directory into archive: `zip -r dirName`

→ will *include* the directory in the archive

files in directory:

1. `cd dirName`
2. `zip ../archive.zip .`

mixe files and directories: `zip archive.zip dirName/ file1 file2`

**Add password**: `-e`

### zsh

alias cheatsheet → <https://github.com/ohmyzsh/ohmyzsh/wiki/Cheatsheet>

| Alias | Function |
|---------|---------|
| `gcommit` | runs `git commit -a -m` and prompts for a message |
| `lc` | launches vscode and opens a folder |
| `upd8` | run system update (based on distro used) |

## Networking

Applies to: [Fedora](../linux/fedora.md)

Connections are stored in: `/etc/NetworkManager/system-connections/`

Activate a connection: `nmcli connection up id 'CONNECTION_ID'`

Can be checked with: `journalctl -xe`

List all: `sudo lshw -class network -short`

## OS

Get full OS info: `cat /etc/os-release`

Get Windows OEM license key: `sudo cat /sys/firmware/acpi/tables/MSDM`

## Printing

Tested with Manjaro only…

`sudo systemctl enable org.cups.cupsd.service` → enable the service

`sudo systemctl start org.cups.cupsd.service` → runs at `localhost:631` now

`sudo systemctl status org.cups.cupsd.service` → check status
