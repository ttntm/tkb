# AdGuard DNS

Using the public AdGuard DNS servers.

## DoH

Use `https://dns.adguard.com/dns-query` in RethinkDNS.

## IPv4

### Default servers

AdGuard DNS will block ads and trackers.

- `94.140.14.14`
- `94.140.15.15`

### Non-filtering servers

AdGuard DNS will not block ads, trackers, or any other DNS requests.

- `94.140.14.140`
- `94.140.14.141`

### Family protection servers

AdGuard DNS will block ads, trackers, adult content, and enable Safe Search and Safe Mode, where possible.

- `94.140.14.15`
- `94.140.15.16`

## IPv6

### Default servers

AdGuard DNS will block ads and trackers.

- `2a10:50c0::ad1:ff`
- `2a10:50c0::ad2:ff`

### Non-filtering servers

AdGuard DNS will not block ads, trackers, or any other DNS requests.

- `2a10:50c0::1:ff`
- `2a10:50c0::2:ff`

### Family protection servers

AdGuard DNS will block ads, trackers, adult content, and enable Safe Search and Safe Mode, where possible.

- `2a10:50c0::bad1:ff`
- `2a10:50c0::bad2:ff`

---

Source: [Connect to public AdGuard DNS server](https://adguard-dns.io/en/public-dns.html)
