# Subscriptions

Software and services I'm paying for.

| Service | Cost | Billing Cycle |
|---------|---------|---------|
| Cell phone plan | €9,99 | Monthly |
| Domain renewals | €54,87 | Annual |
| ISP | €26,70 | Monthly |
| Netflix | €19,99 | Monthly |
| Proton | €115,20 | Annual (Nov.) |
| PS Plus | €59,99 | Annual (July) |

**Annual total**: €888,22 (~€74 per month)

Last update: 10/2024
