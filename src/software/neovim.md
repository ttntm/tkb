# Neovim

Cheat Sheet: <https://quickref.me/vim>

- installation and configuration guide: <https://jdhao.github.io/2018/12/24/centos_nvim_install_use_guide_en/>
- interactive tutorial: [https://openvim.com](https://openvim.com/)

## Commands

exit without saving: `:qa!` or just `:q`

save and exit: `:wq`

**save**: `:w`

**insert**: `insert` or `i`

- insert + newline (above current): `O` (capital O)
- insert + newline (below current): `o` (lowercase o)

append: `a` (end of word) and `A` (EOL)

**delete**: `x` or `entf`

- delete word: `d w`
- delete to end of line: `d $`
- delete whole line: `d d` (can also be repeated, i.e. `2dd` to delete 2 lines etc.)

**copy**: `y` → combine with visual selection mode `v` to select larger amounts of text

- can also be used as an operator like `y w` to copy a whole word etc.

**paste**: `p`

**undo**: `u`

undo whole line: `U`

**redo**: `Ctrl R`

insert from vim register (*after* the cursor): `p` → use together with `dd` to move whole lines around

**replace** (at cursor): `r + newValue`

- replace mode: `R` (until canceled with `ESC`)

**change**: `c + motion` → `c e` to change word until its end

status: `Ctrl g`

**move** in a file:

- start → `gg`
- end → `G`
- line 123 →`123 G`

**search**

- search forward (down from top): `/ term`
- search backward (up from bottom): `? term`
- search next: `n`
- search previous: `N`

→ next/prev work in the direction of the inital search

`Ctrl o` and `Ctrl i` can also be used for back and next jumps respectively

find matching parentheses: `%`

**substitute**: `:s/term/replacement/g` → works on the line the cursor's at; `g` for global, i.e. all occurences of the term if there are more than one

- between line (=line numbers): `:123,132s/term/replacement/g`
- in the whole file: `%s/term/replacement/g`
- in the whole file (with a prompt): `%s/term/replacement/gc`

**run** external commands: `:!command`

visual **selection**: `v` → can be used together with `:w FILENAME` to save parts of a file to a new one

**retrieve** (import at cursor): `:r FILENAME` → can also be used together with `:!command` to place the output of a command into a file as `:r !ls -lh` for example

## Operators & Motions

combine as `operator + motion` → `d $`

short list of motions:

- `w` → until start of next word, excl. its first char
- `e` → until end of current word, incl. last char
- `$` → end of line, incl. last char
- `0` → start of the line

can be used without operator to do just what they say, i.e. jump to EOL

motions can be combined with a number to repeat them, i.e. "move 2 words" as `2w` - this also works together with operators, i.e. `d2w`
