# Software

Software topic index page.

## Pages

- [AdGuard DNS](./adguard-dns.md)
- [Android](./android.md)
- [Commands](./cli.md)
- [Firefox](./firefox.md)
- [Neovim](./neovim.md)
- [Privacy](./privacy.md)
- [Security](./security.md)
- [Subscriptions](./subscriptions.md)
- [VS Code](./vscode.md)

## Alternatives

- [Adobe](images/adobe-alternatives.png) (image)

## App Defaults

| Year | Link |
|---------|---------|
| 2023 | [App Defaults - 2023](https://ttntm.me/blog/app-defaults-2023/) |
| 2024 | [App Defaults - 2024](https://ttntm.me/blog/app-defaults-2024/) |

## Guides

- [DuckDuckGo Bangs](https://duckduckgo.com/bangs)
- [Use Calibre to Remove DRM from Kindle Books and Convert to PDF](https://itsfoss.com/calibre-remove-drm-kindle/)
