# Installation

V8: [Camunda Self-Managed for Absolute Beginners](https://camunda.com/blog/2023/10/camunda-self-managed-for-absolute-beginners/) - working as of Nov. 15 2023.

Alternative: [Camunda Platform 8](https://github.com/camunda/camunda-platform) with plain docker.

## Authentication and authorization

Docs: 

- [Authentication and authorization](https://docs.camunda.io/docs/self-managed/operate-deployment/operate-authentication/)
- [Connect to an OpenID Connect provider](https://docs.camunda.io/docs/self-managed/platform-deployment/helm-kubernetes/guides/connect-to-an-oidc-provider/)

### Identity

Docs: [What is Identity?](https://docs.camunda.io/docs/self-managed/identity/what-is-identity/)

### Keycloak

Docs: 

- [Using existing Keycloak](https://docs.camunda.io/docs/self-managed/platform-deployment/helm-kubernetes/guides/using-existing-keycloak/)
- [Connect to an existing Keycloak instance](https://docs.camunda.io/docs/self-managed/identity/user-guide/configuration/connect-to-an-existing-keycloak/)

## Inbound Connectors (Self Managed)

Default: disabled

Change the default helm chart to get them working:

```yaml
connectors:
  enabled: true
  inbound:
    mode: "credentials"
    auth:
      existingSecret: "demo"
  env:
    - name: CAMUNDA_OPERATE_CLIENT_USERNAME
      value: "demo"
    - name: CAMUNDA_OPERATE_CLIENT_PASSWORD
      value: "demo"
```

Setting `existingSecret` to a value other than `""` was done because helm complained about it. I had to use the `--force` flag when running `helm upgrade` - I did not notice any undesired results of doing that yet.

## Update the Kubernetes Cluster

Force upgrade (via helm): `helm upgrade [RELEASE] [CHART] --force`
