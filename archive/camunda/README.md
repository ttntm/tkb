# Camunda

Camunda index page.

## Pages

- [Architecture](./architecture.md)
- [Development](./development.md)
- [Installation](./installation.md)

## Use Cases

- Microservices Workflow Automation Cheat Sheet – to Centralize or Decentralize?
  - [The Microservices Workflow Automation Cheat Sheet – to Centralize or Decentralize?](https://camunda.com/blog/2020/03/the-microservices-workflow-automation-cheat-sheet-to-centralize-or-decentralize/)
- Workflow engine in your own software?
  - [Enhance Your Products with Workflow Automation](https://camunda.com/solutions/deliver-workflow-automation/)

### Process Orchestration

[The Process Orchestration Handbook](https://camunda.com/process-orchestration/)

Example: [Claims Processing Automation in Insurance: A Complete Guide](https://camunda.com/blog/2023/10/claims-processing-automation-insurance-complete-guide/)

### DMN

Processing of "Business Rules".

Docs: [Business rule tasks](https://docs.camunda.io/docs/components/modeler/bpmn/business-rule-tasks/)
