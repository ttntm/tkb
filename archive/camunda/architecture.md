# Architecture

![camunda platform 8 self-managed architecture diagram](images/camunda-platform-8-self-managed-architecture-diagram.png)

## Using the "Community Edition"

<div class="warning">

**Obsolete as of 04/2024**

See: [Licensing Update for Camunda 8 Self-Managed](https://camunda.com/blog/2024/04/licensing-update-camunda-8-self-managed/).
<br><br>
</div>
<br>

![camunda-platform-8-components](images/camunda-platform-8-components.jpg)

- Green: Open source license (you can use them for free also in production. Commercial support by Camunda available but not mandatory).
- Green stripes: Source-available license (for the curious, the difference is explained below, for most people, there is no real difference).
- Blue: This software is available, but only free for non-production use (e.g. development, test) under the Camunda Self-Managed Free Edition license. If you want to put these components into production, you will need to either use our SaaS service or buy a Camunda Self-Managed Enterprise Edition license.
- Red: Currently only available within the SaaS offering of Camunda 8 and can't be run self-managed. Note: This is subject to change, and red components should turn blue over time.
- Grey: Freely available, also for production use, under the respective licenses of the vendors.

**The resulting architecture without any licensing fee**:

![camunda-platform8-oss](images/camunda-platform8-oss.png)

Operate alternative: [zeebe-simple-monitor](https://github.com/camunda-community-hub/zeebe-simple-monitor)

Source: [How Open is Camunda Platform 8?](https://camunda.com/blog/2022/05/how-open-is-camunda-platform-8/)

--

**BEWARE** - this changed in 04/2024:

![license-changes-2024](./images/license-changes-2024.png)

## Workflow Execution

- [BPMN coverage](https://docs.camunda.io/docs/components/modeler/bpmn/bpmn-coverage/)
- [Message correlation overview](https://docs.camunda.io/docs/components/concepts/messages/#message-correlation-overview)
  - [Message correlation guide](https://docs.camunda.io/docs/guides/message-correlation/)
  - [Zeebe Message Correlation](https://camunda.com/blog/2019/08/zeebe-message-correlation/)
- Loops == [Multi-instance](https://docs.camunda.io/docs/components/modeler/bpmn/multi-instance/)
  - Can be used to process collections of data
- [Boundary events](https://docs.camunda.io/docs/components/modeler/bpmn/events/#boundary-events)
- [Call activities](https://docs.camunda.io/docs/components/modeler/bpmn/call-activities/)
- [User task forms](https://docs.camunda.io/docs/components/modeler/bpmn/user-tasks/#user-task-forms)
- [Workflow patterns](https://docs.camunda.io/docs/components/concepts/workflow-patterns/)

Systems integration example:

![integration](images/camunda_integration.png)

> "glue code" is absolutely necessary and *does not* come out of Camunda by itself

Using Zeebe as middleware:

![zeebe broker alternatives](images/camunda_zeebe-broker-alternatives.png)

There are a couple of **advantages**:

- Less code involved (compare the code of this example to the Kafka/Zeebe example)
- No need to operate an own messaging system or event bus
- Operations tooling from the workflow engine can be used

But there are also **downsides**:

- Dependency to Zeebe in a lot of components (the places where you had a Kafka dependency before)
- Requires confidence on Zeebe to play that central role and take the load

### Connectors

![camunda outbound connectors](images/camunda_outbound_connectors.png)

![camunda inbound connectors](images/camunda_inbound_connectors.png)

Interfaces *do not* work OOTB:

> To create a reusable runtime behavior for your Connector, you are required to implement and expose an implementation of the `OutboundConnectorFunction` interface of the SDK. The Connector runtime environments will call this function; it handles input data, executes the Connector's business logic, and optionally returns a result. Exception handling is optional since the Connector runtime environments handle this as a fallback.

### Call Activity vs. REST Outbound Connector

[Call Activity](https://docs.camunda.io/docs/components/modeler/bpmn/call-activities/): **blocks** the parent process

REST Outbound Connector: **does not block** the parent process

Conclusion: use REST Outbound Connector for "fire and forget" only, using Call Activities is a better option whenever the status of the parent process matters (i.e. should stay "active" in Operate)

### Versioning

> By default, running instances will continue to run on the basis of the version they started with, new instances will be created based on the latest version of that definition.

Docs: [Versioning process definitions](https://docs.camunda.io/docs/components/best-practices/operations/versioning-process-definitions/)

#### Running versions in parallel

Yes, that works; see [Triggering a WF Programmatically](development.html#triggering-a-wf-programmatically)

BUT:

> The big _advantage_ of that default behavior is that you can deploy changed process definitions without caring about running process instances. The process engine is able to manage running instances based on different process definitions in parallel.
>
> The _disadvantage_ is that one needs to deal with the operational complexity of different versions of the process running in parallel as well as the additional complexity in case those processes call subprocesses which have different versions of their own.

## Tasklist

The pre-built standard Tasklist application shows all user tasks that appeared in processes; those processes are running in Zeebe.

It also provides a [REST API](https://docs.camunda.io/docs/next/apis-tools/tasklist-api-rest/tasklist-api-rest-overview/), allowing you to build your own applications or use the general UI that Camunda has prepared for you.

Docs: [Tasklist](https://docs.camunda.io/docs/components/tasklist/introduction-to-tasklist/)
