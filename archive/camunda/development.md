# Development

- [Camunda 8 Self-Managed](https://docs.camunda.io/docs/self-managed/about-self-managed/)
- [Routing events from the outside to the workflow engine](https://docs.camunda.io/docs/components/best-practices/development/routing-events-to-processes/#routing-events-from-the-outside-to-the-workflow-engine)
- [Postman collection](https://www.postman.com/camundateam)

## Connectors

Available connector options are:

- Starting a BPMN process, triggered by external service - use [inbound start event Connector template](https://docs.camunda.io/docs/components/connectors/custom-built-connectors/connector-templates/#inbound-start-event-connector-templates)
- Continue process with an intermediate catch event emitted by external service call - use [inbound intermediate catch event Connector templates](https://docs.camunda.io/docs/components/connectors/custom-built-connectors/connector-templates/#inbound-intermediate-catch-event-connector-templates)
- Trigger an external service - use [outbound Connector template](https://docs.camunda.io/docs/components/connectors/custom-built-connectors/connector-templates/#outbound-connector-templates)

**Self managed modeler**: connector config JSON files have to be placed in the `/camunda-modeler/resources/element-templates/` directory to be made available in the application.

### HTTP/JSON Connector

Config file: [Camunda HTTP JSON Connector](https://github.com/camunda/connectors/tree/main/connectors/http/rest)

_Self managed: Has to be imported into the desktop modeler!_

#### Inbound

```bash
curl -X POST -H "Content-Type: application/json" -d '{"myId": 123456, "myMessage": "Hello, world!"}' http://localhost:8088/inbound/test-webhook
```

Docs: [HTTP Webhook Connector (Inbound)](https://docs.camunda.io/docs/components/connectors/protocol/http-webhook/#verification-expression)

#### Outbound

Docs: [REST Connector (Outbound)](https://docs.camunda.io/docs/components/connectors/protocol/rest/)

### Resources

- [A technical sneak peek into Camunda's connector architecture](https://camunda.com/blog/2022/07/a-technical-sneak-peek-into-camundas-connector-architecture/)
- [Connector SDK](https://docs.camunda.io/docs/components/connectors/custom-built-connectors/connector-sdk/)
- [camunda-8-connectors](https://github.com/camunda-community-hub/camunda-8-connectors) (A curated list of awesome Camunda Platform 8 projects, driven by the community, partners, and Camunda.)
- [Camunda 8 Connector SDK for Node.js ](https://github.com/camunda-community-hub/connector-sdk-nodejs)
  - Example: [camunda-8-connector-openweather-api-nodejs](https://github.com/camunda-community-hub/camunda-8-connector-openweather-api-nodejs)
- [Camunda 8 Connector Job Worker Runtime for Node.js](https://github.com/camunda-community-hub/connector-sdk-nodejs/tree/main/connector-runtime-worker)
- [I want to create REST API in desktop modeler camunda 8](https://forum.camunda.io/t/i-want-to-create-rest-api-in-desktop-modeler-camunda-8/48192/9>)
- [HTTP Webhook Connector URL](https://forum.camunda.io/t/http-webhook-connector-url/42411/6)
- [Enable and Execute Connectors in Camunda 8 Self Managed Installation](https://medium.com/@asriharikiran/enable-and-execute-connectors-in-camunda-8-self-managed-installation-61772acac408)
- [How to build a Camunda 8 Connector using the Node.js SDK](https://medium.com/@sitapati/how-to-build-a-camunda-8-connector-using-the-node-js-sdk-5eb3d798f9ff)

## Kubernetes

Use - actual - `localhost` from inside containers: `host.docker.internal` will do the trick.

## Zeebe

Camunda's workflow engine.

**Service Task** configuration:

- [Service tasks](https://docs.camunda.io/docs/components/modeler/bpmn/service-tasks/)
- Must have a `type` identifier
  - Dynamic values: [Expressions](https://docs.camunda.io/docs/components/concepts/expressions/)
- Can have `taskHeaders`
  - Static metadata handed to workers along with the job
- Local/global variables can be used by the process instance
  - [Input/output variable mappings](https://docs.camunda.io/docs/components/concepts/variables/#inputoutput-variable-mappings)

BUT: needs a custom worker implementation for anything to happen in Zeebe at all!

**User Tasks + Forms**:

Redirect to applications: [Camunda Forum > Camunda Custom Form Key](https://forum.camunda.io/t/camunda-custom-form-key/49670/4)

### Implementation

- [JavaScript](https://docs.camunda.io/docs/apis-tools/community-clients/javascript/)
  - [Zeebe Node.js Client](https://camunda-community-hub.github.io/zeebe-client-node-js/)
  - [Connecting The Workflow Engine With Your World](https://dev.to/camunda/drafting-your-camunda-cloud-architecture-connecting-the-workflow-engine-with-your-world-5110)

#### Job Workers for User Tasks

> a job worker can subscribe to the job type `io.camunda.zeebe:userTask` to complete the job manually.

In your own code: use [The "Decoupled Job Completion" pattern](https://camunda-community-hub.github.io/zeebe-client-node-js/#the-quotdecoupled-job-completionquot-pattern).

Docs: [User tasks](https://docs.camunda.io/docs/components/modeler/bpmn/user-tasks/)

#### Working With Messages

```js
zbc.publishMessage({
  correlationKey: '1234',
  name: 'message-id',
  // variables: {},
  timeToLive: 10000
})
```

`correlationKey` should match a process variable for the message to get correlated to the correct process instance.

Docs: [Publish a Message](https://camunda-community-hub.github.io/zeebe-client-node-js/index.html#publish-a-message)

#### Triggering a WF Programmatically

```js
const zbc = new ZB.ZBClient('localhost:26500')
const result = await zbc.createProcessInstance({
  bpmnProcessId: 'test-process-id',
  variables: {
    key: 'value'
  }
})
```

Docs: [Zeebe Node.js Client > Start a Process Instance](https://camunda-community-hub.github.io/zeebe-client-node-js/#start-a-process-instance)

#### Registering a Worker for Service Tasks

```js
const zbc = new ZB.ZBClient('localhost:26500')
const serviceTaskWorker = zbc.createWorker({
  // `taskType` used here has to match the `type` set for the resp. Service Task
  // in the "Task definition" section in the modeler
  taskType: 'task-type',
  taskHandler: (job) => {
    // worker logic...
    return job.complete({
      result: true
    })
  }
})
```

Docs: [Zeebe Node.js Client > Job Workers](https://camunda-community-hub.github.io/zeebe-client-node-js/#job-workers)

#### Registering a Worker for User Tasks

```js
const zbc = new ZB.ZBClient('localhost:26500')
const userTaskWorker = zbc.createWorker({
  taskType: 'io.camunda.zeebe:userTask',
  taskHandler: (job) => {
    const { key } = job

    console.log('USER TASK INFO: ', {
      correlationId: key
    })

    // worker logic...

    // Call forward() to release worker capacity, assuming async pickup of user tasks
    return job.forward()
  }
})
```
