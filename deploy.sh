#!/bin/bash

rm -R dist

cd cdbrg-pages/
# moving content keeps hidden files/directories (like .git) in place
mkdir deleteme
mv * deleteme
rm -rf deleteme

cd ..

mdbook build

cp favicon.ico dist/
cp favicon.png dist/
cp favicon.svg dist/
cp robots.txt dist/

cp -R dist/. cdbrg-pages/

cd cdbrg-pages/
git add --all
git commit -m "build: update wiki"
git push