window.addEventListener('DOMContentLoaded', () => {
  const chapterItems = Array.from(document.querySelectorAll('.chapter-item'))
  const chapterToggles = Array.from(document.querySelectorAll('.chapter li > a.toggle div'))
  const lastChapter = chapterItems[chapterItems.length-1]
  const links = [
    {
      title: "Repository",
      url: 'https://codeberg.org/ttntm/tkb'
    },
    {
      title: "Tom's Homepage",
      url: 'https://ttntm.me'
    }
  ]

  if (lastChapter) {
    const extraLinks = links.map((link) => {
      return `<li class="chapter-item">
        <a href="${link.url}" rel="noreferrer" target="_blank">${link.title}</a>
      </li>`
    }).join('')

    lastChapter.insertAdjacentHTML('afterend', extraLinks)
  }

  chapterToggles.forEach((el) => {
    el.innerHTML = '&gt;'
    el.style.visibility = 'visible'
  })
})
